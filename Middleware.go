package echomiddleware

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/casbin/casbin"
	"github.com/dgrijalva/jwt-go"
	sentryecho "github.com/getsentry/sentry-go/echo"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"gitlab.com/nikolai.straessle/echomiddleware/jwtmiddleware"
	"gitlab.com/nikolai.straessle/echomiddleware/user"
	"go.elastic.co/apm"
	"io/ioutil"
	"net/http"
	"strconv"
)

var TenantId = uint(0)
var TenandIdField = "tenant_id"

type Enforcer struct {
	Enforcer *casbin.Enforcer
}

func GetDataFromTokenByKey(ctx echo.Context, key string) (map[string]interface{}, error) {
	if nil != ctx.Get("user") {
		user := ctx.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		claimUserToken, ok := claims[key]
		if ok && nil != claimUserToken {
			return claimUserToken.(map[string]interface{}), nil
		}
	}
	return nil, errors.New("key not found")
}

func CheckRole(ctx echo.Context, userKey string, role ...string) error {
	user, err := GetDataFromTokenByKey(ctx, userKey)
	if nil != err {
		ctx.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
	}
	r, ok := user["role"]
	if nil != user && ok && 0 < len(role) {
		for _, inRole := range role {
			if inRole == r {
				return nil
			}
		}
	}
	return echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
}

func SetUserToAPM(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		user, err := GetDataFromTokenByKey(context, "user")
		if nil == err && nil != user {
			tx := apm.TransactionFromContext(context.Request().Context())
			if nil != tx {
				if nil != user["ID"] {
					userIdString := fmt.Sprintf("%v", user["ID"].(float64))
					tx.Context.SetUserID(userIdString)
				}
				if nil != user["username"] {
					username := user["username"].(string)
					tx.Context.SetUsername(username)
				}
				if nil != user["email"] {
					tx.Context.SetUserEmail(user["email"].(string))
				}
				return next(context)
			}
		}
		return next(context)
	}
}

func CheckTenantIDFromHTTPHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		tenantId := context.Request().Header.Get("X-Tenant-ID")
		if "" == tenantId {
			return echo.NewHTTPError(http.StatusBadRequest, "tenant_id missing")
		}
		u64, err := strconv.ParseUint(tenantId, 10, 32)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "invalid tenant_id")
		}
		TenantId = uint(u64)
		return next(context)
	}
}

func CheckTenantID(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		if 0 != user.CurrentUser.ID && TenantId != user.CurrentUser.TenantID {
			return echo.NewHTTPError(http.StatusBadRequest, "tenant id from http-header and jwtmiddleware not equal")
		}
		return next(context)
	}
}

// Enforce check if users role should have access to the function
// JWT middleware is required to add before
func (e *Enforcer) Enforce(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		if 0 == user.CurrentUser.ID {
			return echo.ErrForbidden
		}
		method := ctx.Request().Method
		path := ctx.Request().URL.Path

		result := e.Enforcer.Enforce(user.CurrentUser.Role, path, method)

		if result {
			return next(ctx)
		}
		return echo.ErrForbidden
	}
}

func HTTPErrorHandler(err error, c echo.Context) {
	if hub := sentryecho.GetHubFromContext(c); hub != nil {
		hub.CaptureException(err)
	}
	if gorm.IsRecordNotFoundError(err) {
		err = echo.NewHTTPError(http.StatusNotFound, err.Error())
	}
	he, ok := err.(*echo.HTTPError)
	if ok {
		if he.Internal != nil {
			if herr, ok := he.Internal.(*echo.HTTPError); ok {
				he = herr
			}
		}
	} else {
		he = &echo.HTTPError{
			Code:    http.StatusInternalServerError,
			Message: http.StatusText(http.StatusInternalServerError),
		}
	}
	if m, ok := he.Message.(string); ok {
		he.Message = echo.Map{"message": m}
	}

	// Send response
	if !c.Response().Committed {
		if c.Request().Method == http.MethodHead { // Issue #608
			err = c.NoContent(he.Code)
		} else {
			err = c.JSON(he.Code, he.Message)
		}
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"error":  err,
				"url":    c.Request().URL,
				"header": c.Request().Header,
			}).Error("Could not send error response")
		}
	}
}

func SetCurrentUserMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		return SetCurrentUser(next, context)
	}
}

func SetCurrentUser(next echo.HandlerFunc, context echo.Context) error {
	currentUser, err := GetDataFromTokenByKey(context, "user")
	if nil != err {
		return echo.NewHTTPError(http.StatusBadRequest, "invalid user")
	}
	if nil != currentUser {
		jsonUser, err := json.Marshal(currentUser)
		if nil != err {
			return echo.NewHTTPError(http.StatusBadRequest, "invalid user")
		}
		err = json.Unmarshal(jsonUser, &user.CurrentUser)
		if nil != err {
			return echo.NewHTTPError(http.StatusBadRequest, "invalid user")
		}
	}
	return next(context)
}

func PrepareJWTConfig(jwtType string, jwtKey string, publicKeyPath string) (jwtmiddleware.Config, error) {
	jwtConfig := jwtmiddleware.DefaultJWTConfig
	if "secret" == jwtType {
		jwtConfig.SigningKey = jwtKey
	} else if "rsa" == jwtType {
		data, err := ioutil.ReadFile(publicKeyPath)
		if nil != err {
			return jwtmiddleware.Config{}, err
		}
		publicKey, err := jwt.ParseRSAPublicKeyFromPEM(data)
		if nil != err {
			return jwtmiddleware.Config{}, err
		}
		jwtConfig = jwtmiddleware.Config{
			Skipper: func(c echo.Context) bool {
				return false
			},
			ContextKey:    "user",
			SigningKey:    publicKey,
			SigningMethod: "RS256",
			TokenLookup:   "header:" + echo.HeaderAuthorization,
		}
	}
	return jwtConfig, nil
}

func InitializeAuth(jwtType, jwtKey, publicKeyPath, casbinModel, casbinPolicy, apiKey string, group *echo.Group) error {
	jwtConfig, err := PrepareJWTConfig(jwtType, jwtKey, publicKeyPath)
	if nil != err {
		return err
	}
	group.Use(jwtmiddleware.JWTWithConfig(jwtConfig))
	group.Use(CheckTenantIDFromHTTPHeader)
	group.Use(SetCurrentUserMiddleware)
	group.Use(CheckTenantID)
	enforcer := Enforcer{
		Enforcer: casbin.NewEnforcer(casbinModel, casbinPolicy),
	}

	group.Use(enforcer.Enforce)

	group.Use(middleware.KeyAuth(func(key string, c echo.Context) (bool, error) {
		if 0 == user.CurrentUser.ID {
			return key == apiKey, nil
		}
		return true, nil
	}))
	return nil
}
