package async

import (
	"context"
	"github.com/ThreeDotsLabs/watermill/message"
)

type Async interface {
	Init(driverName, dsn, appName string) (Async, error)
	Subscribe(topicName string, processMsgFunction func(event *message.Message) error, ctx context.Context) error
	Publish(topicName string, payloadInterface interface{}, tenantID uint, messageType, action string, metadata map[string]string) error
}
