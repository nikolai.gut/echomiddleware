package echomiddleware

import (
	sentryecho "github.com/getsentry/sentry-go/echo"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/dominikstraessle/clean/entity"
	"gorm.io/gorm"
	"net/http"
)

func HTTPErrorHandler(err error, c echo.Context) {
	if hub := sentryecho.GetHubFromContext(c); hub != nil {
		hub.CaptureException(err)
	}
	if errors.Is(gorm.ErrRecordNotFound, err) {
		err = echo.NewHTTPError(http.StatusNotFound, err.Error())
	}
	he, ok := err.(*echo.HTTPError)
	if ok {
		if he.Internal != nil {
			if herr, ok := he.Internal.(*echo.HTTPError); ok {
				he = herr
			}
		}
	} else {
		code := http.StatusInternalServerError
		switch err {
		case entity.ErrNotFound:
			code = http.StatusNotFound
			break
		case entity.ErrInvalidEntity:
			code = http.StatusBadRequest
			break
		default:
			code = http.StatusInternalServerError
		}
		he = &echo.HTTPError{
			Code:    code,
			Message: http.StatusText(http.StatusInternalServerError),
		}
	}
	if m, ok := he.Message.(string); ok {
		he.Message = echo.Map{"message": m}
	}

	// Send response
	if !c.Response().Committed {
		if c.Request().Method == http.MethodHead { // Issue #608
			err = c.NoContent(he.Code)
		} else {
			err = c.JSON(he.Code, he.Message)
		}
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"error":  err,
				"url":    c.Request().URL,
				"header": c.Request().Header,
			}).Error("Could not send error response")
		}
	}
}

func AddRequestContextMiddleware(e *echo.Echo, db *gorm.DB) {
	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			db = db.WithContext(context.Request().Context())
			return next(context)
		}
	})
}
