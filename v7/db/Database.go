/*
 * Copyright (c) 2019 Nikolai Strässle.  All rights reserved.
 */

package db

import (
	"crypto/tls"
	"crypto/x509"
	"database/sql"
	"fmt"
	orignalMysql "github.com/go-sql-driver/mysql"
	"github.com/nikolaistraessle/gorm-logrus"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	apmmysql "go.elastic.co/apm/module/apmgormv2/driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"io/ioutil"
	"strings"
)

type Database struct {
	DB            *gorm.DB
	Transaction   *gorm.DB
	TenantId      uint
	TenantIdField string
}

func NewDatabase() (*Database, error) {
	database := Database{}
	dialect, err := CreateSQLDB(viper.GetString("DB.Driver"), viper.GetString("DB.DSN"))
	if err != nil {
		return nil, errors.WithStack(err)
	}
	db, err := gorm.Open(
		dialect,
		&gorm.Config{
			Logger: gormlogrus.New(),
		},
	)
	if nil != err {
		return nil, err
	}
	database.DB = db

	return &database, nil
}

func CreateSQLDB(driverName, dsn string) (gorm.Dialector, error) {
	dsn, err := RegisterSSL(dsn)
	if nil != err {
		return nil, errors.WithStack(err)
	}
	dialect := apmmysql.Open(dsn)
	if !strings.Contains(dsn, ":") {
		dialect = mysql.New(mysql.Config{
			DriverName: driverName,
			DSN:        dsn,
		})
	}
	return dialect, nil
}

func (db *Database) Clone() *Database {
	return &Database{
		DB:            db.DB,
		TenantId:      db.TenantId,
		TenantIdField: db.TenantIdField,
	}
}

func RegisterSSL(dsn string) (string, error) {
	if strings.Contains(dsn, "&ssl-mode=REQUIRED") {
		rootCertPool := x509.NewCertPool()
		pem, err := ioutil.ReadFile(viper.GetString("DB.PathToCert"))
		if err != nil {
			return "", errors.WithStack(err)
		}
		if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
			return "", errors.New("Failed to append PEM.")
		}
		return strings.ReplaceAll(dsn, "&ssl-mode=REQUIRED", ""), orignalMysql.RegisterTLSConfig("custom", &tls.Config{
			RootCAs: rootCertPool,
		})
	}
	return dsn, nil
}

func (db *Database) GetStatus() (sql.DBStats, error) {
	database, err := db.DB.DB()
	if nil != err {
		return sql.DBStats{}, err
	}
	if err := database.Ping(); nil != err {
		return sql.DBStats{}, err
	}
	return database.Stats(), nil
}

func (db *Database) GetCurrentTransaction() *gorm.DB {
	useDatabase := db.DB
	if nil != db.Transaction {
		useDatabase = db.Transaction
	}
	return useDatabase
}

func (db *Database) Migrate(values ...interface{}) error {
	if err := db.DB.AutoMigrate(values...); nil != err {
		return err
	}
	return nil
}

func (db *Database) CreateDBQueryWithDb(model interface{}, userDatabase *gorm.DB) *gorm.DB {
	return db.CreateDBQueryWithTenantIdAndTenantIdFieldAndDB(model, db.TenantId, db.TenantIdField, userDatabase)
}

func (db *Database) CreateDBQueryWithTenantId(model interface{}, tenantId uint) *gorm.DB {
	return db.CreateDBQueryWithTenantIdAndTenantIdField(model, tenantId, db.TenantIdField)
}

func (db *Database) CreateDBQueryWithTenantIdAndTenantIdField(model interface{}, tenantId uint, tenantIdField string) *gorm.DB {
	return db.CreateDBQueryWithTenantIdAndTenantIdFieldAndDB(model, tenantId, tenantIdField, db.GetCurrentTransaction())
}

func (db *Database) CreateDBQueryWithTenantIdAndTenantIdFieldAndDB(model interface{}, tenantId uint, tenantIdField string, useDatabase *gorm.DB) *gorm.DB {
	if nil != model {
		useDatabase = useDatabase.Model(model)
	}
	if 0 != tenantId && "" != tenantIdField {
		stmt := &gorm.Statement{DB: db.DB}
		_ = stmt.Parse(model)
		tableName := stmt.Schema.Table
		return useDatabase.Where(fmt.Sprintf("%s.%s = ? OR %[1]s.%[2]s = 0", tableName, tenantIdField), tenantId)
	}
	return useDatabase
}

func (db *Database) CreateInsertDBQuery(model interface{}) *gorm.DB {
	return db.CreateInsertDBQueryWithDB(model, db.GetCurrentTransaction())
}

func (db *Database) CreateInsertDBQueryWithDB(model interface{}, userDatabase *gorm.DB) *gorm.DB {
	if nil != model {
		userDatabase = userDatabase.Model(model)
	}
	return userDatabase
}

func (db *Database) BeginTransaction() {
	if nil != db.Transaction {
		return
	}
	db.Transaction = db.DB.Begin(&sql.TxOptions{
		Isolation: sql.LevelReadCommitted,
	})
}

func (db *Database) CommitTransaction() error {
	if nil != db.Transaction {
		err := db.Transaction.Commit().Error
		db.Transaction = nil
		return err
	}
	return nil
}

func (db *Database) Rollback() error {
	if nil != db.Transaction {
		err := db.Transaction.Rollback().Error
		db.Transaction = nil
		return err
	}
	return nil
}

func (db *Database) RemoveDefaultFields(fields map[string]interface{}) map[string]interface{} {
	// remove default fields
	_, ok := fields["ID"]
	if ok {
		delete(fields, "ID")
	}
	_, ok = fields["id"]
	if ok {
		delete(fields, "id")
	}
	_, ok = fields["CreatedAt"]
	if ok {
		delete(fields, "CreatedAt")
	}
	_, ok = fields["created_at"]
	if ok {
		delete(fields, "created_at")
	}
	_, ok = fields["DeletedAt"]
	if ok {
		delete(fields, "DeletedAt")
	}
	_, ok = fields["deleted_at"]
	if ok {
		delete(fields, "deleted_at")
	}
	_, ok = fields["UpdatedAt"]
	if ok {
		delete(fields, "UpdatedAt")
	}
	_, ok = fields["updated_at"]
	if ok {
		delete(fields, "updated_at")
	}
	return fields
}
