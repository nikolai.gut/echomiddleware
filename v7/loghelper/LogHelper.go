package loghelper

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
	"strings"
)

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func LogError(fields logrus.Fields, err error) {
	if stackErr, ok := err.(stackTracer); ok {
		fields["stacktrance"] = stackErr.StackTrace()
	}
	logrus.WithFields(fields).Error(err)
}

func PrepareLogger(e *echo.Echo) error {
	e.Logger.SetLevel(99)
	logrus.SetFormatter(&logrus.JSONFormatter{})
	if strings.Contains(viper.GetString("Log.Path"), "stdout") {
		logrus.SetOutput(os.Stdout)
	} else {
		f, err := os.OpenFile(viper.GetString("Log.Path"), os.O_WRONLY|os.O_CREATE, 0600)
		if err != nil {
			return err
		}
		logrus.SetOutput(f)
	}

	switch viper.GetString("Log.Level") {
	case "DEBUG":
		logrus.SetLevel(logrus.DebugLevel)
	case "INFO":
		logrus.SetLevel(logrus.InfoLevel)
	case "WARN":
		logrus.SetLevel(logrus.WarnLevel)
	default:
		logrus.SetLevel(logrus.ErrorLevel)
	}
	return nil
}
