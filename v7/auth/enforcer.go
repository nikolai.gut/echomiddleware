package auth

import (
	"github.com/casbin/casbin"
	"github.com/labstack/echo/v4"
)

type Enforcer struct {
	Enforcer *casbin.Enforcer
}

// Enforce check if users role should have access to the function
// JWT middleware is required to add before
func (e *Enforcer) Enforce(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		claim := GetClaim(ctx)
		method := ctx.Request().Method
		path := ctx.Request().URL.Path

		allowed := e.Enforcer.Enforce(claim.User.Role, path, method)

		if allowed {
			return next(ctx)
		}
		return echo.ErrForbidden
	}
}
