// +build !integration

package auth_test

import (
	"fmt"
	"github.com/casbin/casbin"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikolai.straessle/echomiddleware/v7/auth"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

type test struct {
	user   *auth.User
	path   string
	want   int
	method string
}

func Test_Enforcer(t *testing.T) {
	e := echo.New()
	file, err := ioutil.ReadFile(absPath("test/public.pem"))
	assert.NoError(t, err)
	key, err := jwt.ParseRSAPublicKeyFromPEM(file)
	assert.NoError(t, err)
	config := auth.JWTConfig{
		SigningKey: key,
		AllowNoJWT: true,
	}
	e.Use(auth.JWTWithConfig(config))
	e.Use(auth.CheckJwtAndTenant)
	model := absPath("test/casbin/rbac_model.conf")
	policy := absPath("test/casbin/policy.csv")

	enforcer := auth.Enforcer{
		Enforcer: casbin.NewEnforcer(model, policy),
	}
	e.Use(enforcer.Enforce)

	e.Any("/*", func(c echo.Context) error {
		return c.NoContent(http.StatusOK)
	})

	cases := []test{
		{
			user:   newSuperAdminFixture(),
			path:   "/v1/documents",
			method: http.MethodGet,
			want:   http.StatusOK,
		},
		{
			user:   newSuperAdminFixture(),
			path:   "/v1/documents",
			method: http.MethodPost,
			want:   http.StatusOK,
		},
		{
			user:   newSuperAdminFixture(),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodGet,
			want:   http.StatusOK,
		},
		{
			user:   newSuperAdminFixture(),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodPut,
			want:   http.StatusOK,
		},
		{
			user:   newSuperAdminFixture(),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodDelete,
			want:   http.StatusOK,
		},
		{
			user:   newSuperAdminFixture(),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289/render",
			method: http.MethodPost,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_admin"),
			path:   "/v1/documents",
			method: http.MethodGet,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_admin"),
			path:   "/v1/documents",
			method: http.MethodPost,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_admin"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodGet,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_admin"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodPut,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_admin"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodDelete,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_admin"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289/render",
			method: http.MethodPost,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_user"),
			path:   "/v1/documents",
			method: http.MethodGet,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_user"),
			path:   "/v1/documents",
			method: http.MethodPost,
			want:   http.StatusForbidden,
		},
		{
			user:   newRoleFixture("tenant_user"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodGet,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_user"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodPut,
			want:   http.StatusForbidden,
		},
		{
			user:   newRoleFixture("tenant_user"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodDelete,
			want:   http.StatusForbidden,
		},
		{
			user:   newRoleFixture("tenant_user"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289/render",
			method: http.MethodPost,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_school_admin"),
			path:   "/v1/documents",
			method: http.MethodGet,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_school_admin"),
			path:   "/v1/documents",
			method: http.MethodPost,
			want:   http.StatusForbidden,
		},
		{
			user:   newRoleFixture("tenant_school_admin"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodGet,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("tenant_school_admin"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodPut,
			want:   http.StatusForbidden,
		},
		{
			user:   newRoleFixture("tenant_school_admin"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodDelete,
			want:   http.StatusForbidden,
		},
		{
			user:   newRoleFixture("tenant_school_admin"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289/render",
			method: http.MethodPost,
			want:   http.StatusOK,
		},
		{
			// an anonymous fixture here is valid, as the jwt middleware would set the role of the anonymous user anyway
			user:   newRoleFixture("anonymous"),
			path:   "/v1/documents",
			method: http.MethodGet,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("anonymous"),
			path:   "/v1/documents",
			method: http.MethodPost,
			want:   http.StatusForbidden,
		},
		{
			user:   newRoleFixture("anonymous"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodGet,
			want:   http.StatusOK,
		},
		{
			user:   newRoleFixture("anonymous"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodPut,
			want:   http.StatusForbidden,
		},
		{
			user:   newRoleFixture("anonymous"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289",
			method: http.MethodDelete,
			want:   http.StatusForbidden,
		},
		{
			user:   newRoleFixture("anonymous"),
			path:   "/v1/documents/abd30af8-b266-42d0-8c48-820a93c1e289/render",
			method: http.MethodPost,
			want:   http.StatusOK,
		},
	}

	for _, tc := range cases {
		t.Run(getDescription(tc), func(t *testing.T) {
			req := httptest.NewRequest(tc.method, tc.path, nil)
			userFixture := tc.user
			req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+newJwtFixture(userFixture))
			req.Header.Set(auth.TenantHeader, strconv.Itoa(int(userFixture.TenantID)))
			w := httptest.NewRecorder()

			e.ServeHTTP(w, req)

			assert.Equal(t, tc.want, w.Code)
		})
	}
}

func getDescription(tc test) string {
	return fmt.Sprintf("%s on %s %s is %s", tc.user.Role, tc.method, tc.path, http.StatusText(tc.want))
}
