// +build !integration

package auth_test

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikolai.straessle/echomiddleware/v7/auth"
	"gitlab.com/nikolai.straessle/gotestutils"
	"gorm.io/gorm"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"runtime"
	"strconv"
	"strings"
	"testing"
	"time"
)

func Test_RegisterAuth(t *testing.T) {
	e := echo.New()
	v1 := e.Group("/")
	apiKey := "test"

	type test struct {
		pubKey  string
		model   string
		policy  string
		desc    string
		wantErr bool
	}
	cases := []test{
		{
			desc:    "valid",
			pubKey:  absPath("test/public.pem"),
			model:   absPath("test/casbin/rbac_model.conf"),
			policy:  absPath("test/casbin/policy.csv"),
			wantErr: false,
		},
		{
			desc:    "invalid pubkey file content",
			pubKey:  absPath("test/casbin/rbac_model.conf"),
			model:   absPath("test/casbin/rbac_model.conf"),
			policy:  absPath("test/casbin/policy.csv"),
			wantErr: true,
		},
		{
			desc:    "invalid pubkey file",
			pubKey:  "invalid",
			model:   absPath("test/casbin/rbac_model.conf"),
			policy:  absPath("test/casbin/policy.csv"),
			wantErr: true,
		},
	}
	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			err := auth.RegisterAuth(
				tc.pubKey,
				tc.model,
				tc.policy,
				apiKey,
				v1)
			if tc.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func Test_ApiKeyMiddleware(t *testing.T) {
	e := echo.New()
	e.GET("/", func(context echo.Context) error {
		return context.NoContent(http.StatusOK)
	})

	file, err := ioutil.ReadFile(absPath("test/public.pem"))
	assert.NoError(t, err)
	key, err := jwt.ParseRSAPublicKeyFromPEM(file)
	assert.NoError(t, err)
	config := auth.JWTConfig{
		SigningKey: key,
		AllowNoJWT: true,
	}
	e.Use(auth.JWTWithConfig(config))
	e.Use(auth.CheckJwtAndTenant)
	apiKey := "test"
	e.Use(middleware.KeyAuth(auth.KeyFunc(apiKey)))

	type test struct {
		desc string
		req  func() *http.Request
		want int
	}

	cases := []test{
		{
			desc: "valid admin",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				adminFixture := newSuperAdminFixture()
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+newJwtFixture(adminFixture))
				req.Header.Set(auth.TenantHeader, strconv.Itoa(int(adminFixture.TenantID)))
				return req
			},
			want: http.StatusOK,
		},
		{
			desc: "valid anonymous",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+apiKey)
				req.Header.Set(auth.TenantHeader, "1")
				return req
			},
			want: http.StatusOK,
		},
		{
			desc: "invalid anonymous",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+apiKey)
				req.Header.Set(auth.TenantHeader, "1")
				return req
			},
			want: http.StatusOK,
		},
	}

	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			w := httptest.NewRecorder()

			e.ServeHTTP(w, tc.req())

			assert.Equal(t, tc.want, w.Code)
		})
	}
}

func Test_JWT_Cases(t *testing.T) {
	e := echo.New()
	e.GET("/", func(context echo.Context) error {
		return context.NoContent(http.StatusOK)
	})

	file, err := ioutil.ReadFile(absPath("test/public.pem"))
	assert.NoError(t, err)
	key, err := jwt.ParseRSAPublicKeyFromPEM(file)
	assert.NoError(t, err)
	config := auth.JWTConfig{
		SigningKey: key,
		AllowNoJWT: true,
	}
	e.Use(auth.JWTWithConfig(config))
	e.Use(auth.CheckJwtAndTenant)

	type test struct {
		desc string
		req  func() *http.Request
		want int
	}

	cases := []test{
		{
			desc: "valid admin",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				adminFixture := newSuperAdminFixture()
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+newJwtFixture(adminFixture))
				req.Header.Set(auth.TenantHeader, strconv.Itoa(int(adminFixture.TenantID)))
				return req
			},
			want: http.StatusOK,
		},
		{
			desc: "admin with different tenants",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				adminFixture := newSuperAdminFixture()
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+newJwtFixture(adminFixture))
				req.Header.Set(auth.TenantHeader, strconv.Itoa(int(adminFixture.TenantID)+1))
				return req
			},
			want: http.StatusOK,
		},
		{
			desc: "admin with missing tenant",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				adminFixture := newSuperAdminFixture()
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+newJwtFixture(adminFixture))
				return req
			},
			want: http.StatusBadRequest,
		},
		{
			desc: "valid user",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				userFixture := newUserFixture()
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+newJwtFixture(userFixture))
				req.Header.Set(auth.TenantHeader, strconv.Itoa(int(userFixture.TenantID)))
				return req
			},
			want: http.StatusOK,
		},
		{
			desc: "user with wrong signed jwt",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				userFixture := newUserFixture()
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+newHS256JWT())
				req.Header.Set(auth.TenantHeader, strconv.Itoa(int(userFixture.TenantID)))
				return req
			},
			want: http.StatusOK,
		},
		{
			desc: "user with different tenants",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				userFixture := newUserFixture()
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+newJwtFixture(userFixture))
				req.Header.Set(auth.TenantHeader, strconv.Itoa(int(userFixture.TenantID)+1))
				return req
			},
			want: http.StatusBadRequest,
		},
		{
			desc: "user with missing tenant header",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				userFixture := newUserFixture()
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+newJwtFixture(userFixture))
				return req
			},
			want: http.StatusBadRequest,
		},
		{
			desc: "user with header tenant 0",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				userFixture := newUserFixture()
				req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" "+newJwtFixture(userFixture))
				req.Header.Set(auth.TenantHeader, "0")
				return req
			},
			want: http.StatusBadRequest,
		},
		{
			desc: "valid anonymous",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				req.Header.Set(auth.TenantHeader, "1")
				return req
			},
			want: http.StatusOK,
		},
		{
			desc: "anonymous with header tenant 0",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				req.Header.Set(auth.TenantHeader, "0")
				return req
			},
			want: http.StatusBadRequest,
		},
		{
			desc: "anonymous with missing tenant header",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				return req
			},
			want: http.StatusBadRequest,
		},
		{
			desc: "anonymous with invalid tenant hader (not uint)",
			req: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				req.Header.Set(auth.TenantHeader, "-99")
				return req
			},
			want: http.StatusBadRequest,
		},
	}

	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			w := httptest.NewRecorder()

			e.ServeHTTP(w, tc.req())

			assert.Equal(t, tc.want, w.Code)
		})
	}
}

func Test_Config_Variants(t *testing.T) {
	t.Run("without signing key", func(t *testing.T) {
		e := echo.New()

		config := auth.JWTConfig{
			AllowNoJWT: true,
		}
		assert.Panics(t, func() {
			e.Use(auth.JWTWithConfig(config))
		})
	})
	t.Run("with AllowNoJWT set to false and invalid jwt", func(t *testing.T) {
		e := echo.New()
		e.GET("/", func(context echo.Context) error {
			return context.NoContent(http.StatusOK)
		})

		file, err := ioutil.ReadFile(absPath("test/public.pem"))
		assert.NoError(t, err)
		key, err := jwt.ParseRSAPublicKeyFromPEM(file)
		assert.NoError(t, err)
		config := auth.JWTConfig{
			SigningKey: key,
			AllowNoJWT: false,
		}
		e.Use(auth.JWTWithConfig(config))
		e.Use(auth.CheckJwtAndTenant)

		req := httptest.NewRequest(http.MethodGet, "/", nil)
		userFixture := newUserFixture()
		req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" invalid jwt")
		req.Header.Set(auth.TenantHeader, strconv.Itoa(int(userFixture.TenantID)))
		w := httptest.NewRecorder()

		e.ServeHTTP(w, req)

		assert.Equal(t, http.StatusUnauthorized, w.Code)
	})
	t.Run("with AllowNoJWT set to false and missing header", func(t *testing.T) {
		e := echo.New()
		e.GET("/", func(context echo.Context) error {
			return context.NoContent(http.StatusOK)
		})

		file, err := ioutil.ReadFile(absPath("test/public.pem"))
		assert.NoError(t, err)
		key, err := jwt.ParseRSAPublicKeyFromPEM(file)
		assert.NoError(t, err)
		config := auth.JWTConfig{
			SigningKey: key,
			AllowNoJWT: false,
		}
		e.Use(auth.JWTWithConfig(config))
		e.Use(auth.CheckJwtAndTenant)

		req := httptest.NewRequest(http.MethodGet, "/", nil)
		w := httptest.NewRecorder()

		e.ServeHTTP(w, req)

		assert.Equal(t, http.StatusBadRequest, w.Code)
	})
	t.Run("with AllowNoJWT set to true and invalid jwt", func(t *testing.T) {
		e := echo.New()
		e.GET("/", func(context echo.Context) error {
			return context.NoContent(http.StatusOK)
		})

		file, err := ioutil.ReadFile(absPath("test/public.pem"))
		assert.NoError(t, err)
		key, err := jwt.ParseRSAPublicKeyFromPEM(file)
		assert.NoError(t, err)
		config := auth.JWTConfig{
			SigningKey: key,
			AllowNoJWT: true,
		}
		e.Use(auth.JWTWithConfig(config))
		e.Use(auth.CheckJwtAndTenant)

		req := httptest.NewRequest(http.MethodGet, "/", nil)
		userFixture := newUserFixture()
		req.Header.Set(echo.HeaderAuthorization, auth.JwtHeaderPrefix+" invalid jwt")
		req.Header.Set(auth.TenantHeader, strconv.Itoa(int(userFixture.TenantID)))
		w := httptest.NewRecorder()

		e.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)
	})
}

func absPath(path string) string {
	const currentFilePath = "jwt_test.go"
	_, abs, _, _ := runtime.Caller(0)

	file := strings.TrimSuffix(abs, currentFilePath)
	file += path
	return file
}

func newSuperAdminFixture() *auth.User {
	return &auth.User{
		ID:           1,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		DeletedAt:    gorm.DeletedAt{},
		Username:     "dominikstr",
		FirstName:    "Administrator",
		LastName:     "Blub",
		Email:        "blub@admin.io",
		Phone:        "+41786830499",
		Role:         "admin",
		SchoolMember: false,
		CompanyID:    11,
		TenantID:     0,
	}
}

func newUserFixture() *auth.User {
	return newRoleFixture("tenant_admin")
}

func newRoleFixture(role string) *auth.User {
	return &auth.User{
		ID:           1,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		DeletedAt:    gorm.DeletedAt{},
		Username:     "dominikstr",
		FirstName:    "Dominik",
		LastName:     "Strässle",
		Email:        "dominik@straessle.me",
		Phone:        "+41786830456",
		Role:         role,
		SchoolMember: false,
		CompanyID:    11,
		TenantID:     22,
	}
}

func newHS256JWT() string {
	return `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjI1ODcyMTUyOTYsImlhdCI6MTU4NzEyODg5NiwidXNlciI6eyJJRCI6MSwiQ3JlYXRlZEF0IjoiMjAyMS0wMy0wOFQxNToxNDo1NS4xMTA4NjIxKzAxOjAwIiwiVXBkYXRlZEF0IjoiMjAyMS0wMy0wOFQxNToxNDo1NS4xMTA4NjIxNjkrMDE6MDAiLCJEZWxldGVkQXQiOm51bGwsInVzZXJuYW1lIjoiZG9taW5pa3N0ciIsImZpcnN0X25hbWUiOiJEb21pbmlrIiwibGFzdF9uYW1lIjoiU3Ryw6Rzc2xlIiwiZW1haWwiOiJkb21pbmlrQHN0cmFlc3NsZS5tZSIsInBob25lIjoiKzQxNzg2ODMwNDU2Iiwicm9sZSI6IiIsImxhc3RfcGFzc3dvcmRfY2hhbmdlIjpudWxsLCJ0d2FfbWV0aG9kIjoiIiwic2Nob29sX21lbWJlciI6ZmFsc2UsImp3dCI6IiIsImNvbXBhbnlfaWQiOjExLCJ0ZW5hbnRfaWQiOjIyfX0.bOSMKUvkKVU-ZK9jYAiI21j-VbaZXVicr8M8amssQ-Q`
}

func newJwtFixture(user *auth.User) string {
	claims := &auth.Claim{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
		User: *user,
	}

	file, err := ioutil.ReadFile(absPath("test/key.pem"))
	if err != nil {
		panic(err)
	}
	key, err := jwt.ParseRSAPrivateKeyFromPEM(file)
	if err != nil {
		panic(err)
	}

	// Create token with claims
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	// Generate encoded token and send it as response.
	t, err := token.SignedString(key)
	if err != nil {
		panic(err)
	}

	return t
}

func TestCheckIfPathIsInExcludedList(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "TestCheckIfPathIsInExcludedList",
			args: args{
				path: "/cronjob/",
			},
			want: true,
		},
		{
			name: "TestCheckIfPathIsInExcludedList",
			args: args{
				path: "/aba/",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := gotestutils.CreateEchoCtxFromRequest(gotestutils.Request{})
			ctx.SetPath(tt.args.path)
			if got := auth.CheckIfPathIsInExcludedList(ctx); got != tt.want {
				t.Errorf("CheckIfPathIsInExcludedList() = %v, want %v", got, tt.want)
			}
		})
	}
}
