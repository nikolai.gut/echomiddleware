/*
 * Copyright (c) 2019 Nikolai Strässle.  All rights reserved.
 */

package db

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	"github.com/labstack/gommon/log"
	"github.com/sirupsen/logrus"
	"gitlab.com/nikolai.straessle/echomiddleware/v3"
	_ "go.elastic.co/apm"
	"go.elastic.co/apm/module/apmgorm"
	_ "go.elastic.co/apm/module/apmgorm/dialects/mysql"
	_ "go.elastic.co/apm/module/apmgorm/dialects/postgres"
)

var DB Database

type Database struct {
	Database     func() (*gorm.DB, error)
	DatabaseGorm *gorm.DB
	DriverName   string
	Username     string
	Password     string
	Host         string
	Port         int
	DatabaseName string
	DatabaseFile string
	Context      context.Context
	Transaction  *gorm.DB
}

func (db *Database) Init() {
	db.Database = func() (*gorm.DB, error) {
		var err error
		if nil != db.DatabaseGorm {
			err = db.DatabaseGorm.DB().Ping()
		} else {
			err = errors.New("no database connection")
		}
		if nil != err {
			connectionString, err := db.CreateDatabaseConnectionString()
			if nil != err {
				return nil, err
			}
			var gormDb *gorm.DB
			if "mssql" == db.DriverName {
				gormDb, err = gorm.Open(db.DriverName, connectionString)
			} else {
				gormDb, err = apmgorm.Open(db.DriverName, connectionString)
			}
			if nil != err {
				return nil, err
			}
			db.DatabaseGorm = gormDb
		}
		if nil != db.Context {
			db.DatabaseGorm = apmgorm.WithContext(db.Context, db.DatabaseGorm)
		}
		return db.DatabaseGorm, nil
	}
}

func (db *Database) SetContext(ctx context.Context, callback func()) {
	if nil != ctx && nil != db {
		db.Context = ctx
		callback()
	}
}

func (db *Database) CreateDatabaseConnectionString() (connectionString string, err error) {
	switch db.DriverName {
	case "sqlite3":
		return "", errors.New("sqlite3 is not supported at the moment")
	case "mysql":
		if "" == db.DatabaseName {
			return "", errors.New("no database name given")
		}
		if "" == db.Host {
			log.Warnf("no host given for driver %s", db.DriverName)
			db.Host = "localhost"
		}
		if 0 == db.Port {
			log.Warnf("no port given for driver %s", db.DriverName)
			db.Port = 3306
		}
		conString := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", db.Username, db.Password, db.Host, db.Port, db.DatabaseName)
		return conString, nil
	case "postgres":
		if "" == db.DatabaseName {
			return "", errors.New("no database name given")
		}
		if "" == db.Host {
			log.Warnf("no host given for driver %s", db.DriverName)
			db.Host = "localhost"
		}
		if 0 == db.Port {
			log.Warnf("no port given for driver %s", db.DriverName)
			db.Port = 5432
		}
		conString := fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s", db.Host, db.Port, db.Username, db.DatabaseName, db.Password)
		return conString, nil
	case "mssql":
		if "" == db.DatabaseName {
			return "", errors.New("no database name given")
		}
		if "" == db.Host {
			log.Warnf("no host given for driver %s", db.DriverName)
			db.Host = "localhost"
		}
		if 0 == db.Port {
			log.Warnf("no port given for driver %s", db.DriverName)
			db.Port = 1433
		}
		conString := fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s", db.Username, db.Password, db.Host, db.Port, db.DatabaseName)
		return conString, nil
	}
	return "", fmt.Errorf("invalid database driver given: %s", db.DriverName)
}

func (db *Database) Migrate(values ...interface{}) error {
	if nil == db.DatabaseGorm {
		db.Init()
	}
	database, err := db.Database()
	defer db.CloseDBAndHandleError()
	if nil != err {
		return err
	}
	database.AutoMigrate(values...)
	dbErrors := database.GetErrors()
	if nil != dbErrors && 0 < len(dbErrors) {
		return dbErrors[0]
	}
	return nil
}

func (db *Database) CloseDBAndHandleError() {
	if nil != db.DatabaseGorm {
		err := db.DatabaseGorm.Close()
		if nil != err {
			panic(err)
		}
	}
}

func CreateDBQuery(model interface{}) *gorm.DB {
	var db *gorm.DB
	if nil == DB.DatabaseGorm {
		_, err := DB.Database()
		if nil != err {
			logrus.WithFields(logrus.Fields{
				"error": err,
			}).Error("could not connect to database")
		}
	}
	if nil == DB.Transaction {
		if err := DB.DatabaseGorm.DB().Ping(); nil != err {
			_, err := DB.Database()
			if nil != err {
				logrus.WithFields(logrus.Fields{
					"error": err,
				}).Error("could not connect to database")
			}
		}
		db = DB.DatabaseGorm
	} else {
		db = DB.Transaction
	}
	if nil != model {
		db = db.Model(model)
	}
	if 0 != echomiddleware.TenantId && "" != echomiddleware.TenandIdField {
		table := db.NewScope(model).GetModelStruct().TableName(db)
		return db.Where(fmt.Sprintf("%s.%s = ? OR %[1]s.%[2]s = 0", table, echomiddleware.TenandIdField), echomiddleware.TenantId)
	}
	return db
}

func CreateInsertDBQuery(model interface{}) *gorm.DB {
	var db *gorm.DB
	if nil == DB.DatabaseGorm {
		_, err := DB.Database()
		if nil != err {
			logrus.WithFields(logrus.Fields{
				"error": err,
			}).Error("could not connect to database")
		}
	}
	if nil == DB.Transaction {
		if err := DB.DatabaseGorm.DB().Ping(); nil != err {
			_, err := DB.Database()
			if nil != err {
				logrus.WithFields(logrus.Fields{
					"error": err,
				}).Error("could not connect to database")
			}
		}
		db = DB.DatabaseGorm
	} else {
		db = DB.Transaction
	}
	if nil != model {
		db = db.Model(model)
	}
	return db
}

func BeginTransaction() {
	if nil == DB.DatabaseGorm {
		_, err := DB.Database()
		if nil != err {
			logrus.WithFields(logrus.Fields{
				"error": err,
			}).Error("could not connect to database")
		}
	}
	if err := DB.DatabaseGorm.DB().Ping(); nil != err {
		_, err := DB.Database()
		if nil != err {
			logrus.WithFields(logrus.Fields{
				"error": err,
			}).Error("could not connect to database")
		}
	}
	if "mysql" == DB.DriverName {
		DB.Transaction = DB.DatabaseGorm.BeginTx(context.Background(), &sql.TxOptions{
			Isolation: sql.LevelReadCommitted,
		})
	} else {
		DB.Transaction = DB.DatabaseGorm.Begin()
	}
}

func CommitTransaction() error {
	if nil != DB.Transaction {
		err := DB.Transaction.Commit().Error
		DB.Transaction = nil
		return err
	}
	return nil
}

func RollbacTransactionkUnlessCommitted() error {
	if nil != DB.Transaction {
		err := DB.Transaction.RollbackUnlessCommitted().Error
		DB.Transaction = nil
		return err
	}
	return nil
}

func Rollback() error {
	if nil != DB.Transaction {
		err := DB.Transaction.Rollback().Error
		DB.Transaction = nil
		return err
	}
	return nil
}

func RemoveDefaultFields(fields map[string]interface{}) map[string]interface{} {
	// remove default fields
	_, ok := fields["ID"]
	if ok {
		delete(fields, "ID")
	}
	_, ok = fields["id"]
	if ok {
		delete(fields, "id")
	}
	_, ok = fields["CreatedAt"]
	if ok {
		delete(fields, "CreatedAt")
	}
	_, ok = fields["created_at"]
	if ok {
		delete(fields, "created_at")
	}
	_, ok = fields["DeletedAt"]
	if ok {
		delete(fields, "DeletedAt")
	}
	_, ok = fields["deleted_at"]
	if ok {
		delete(fields, "deleted_at")
	}
	_, ok = fields["UpdatedAt"]
	if ok {
		delete(fields, "UpdatedAt")
	}
	_, ok = fields["updated_at"]
	if ok {
		delete(fields, "updated_at")
	}
	return fields
}
