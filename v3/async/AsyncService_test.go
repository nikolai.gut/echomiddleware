package async

import (
	"context"
	"errors"
	"fmt"
	"github.com/kubemq-io/kubemq-go"
	pb "github.com/kubemq-io/protobuf/go"
	"reflect"
	"testing"
)

func init() {
	SetTestRun()
}

func TestPublishMessage(t *testing.T) {
	type args struct {
		channel     string
		host        string
		clientId    string
		port        int
		msg         interface{}
		tenantID    uint
		messageType string
		action      string
		metadata    map[string]string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "TestPublishMessage",
			args: args{
				host:     "localhost",
				clientId: "test",
				port:     5555,
				channel:  "tst",
				msg:      true,
				metadata: map[string]string{"heritage_0": "", "target_0": ""},
			},
			wantErr: false,
		},
		{
			name: "TestPublishMessageInvalidMessage",
			args: args{
				host:     "localhostaaa",
				clientId: "test",
				port:     5555,
				channel:  "tst",
				msg:      func() {},
			},
			wantErr: true,
		},
		{
			name: "TestPublishMessageInvalidURI",
			args: args{
				host:     "localhostadsfasfd",
				clientId: "test",
				port:     5555,
				channel:  "tst",
				msg:      "test",
			},
			wantErr: true,
		},
		{
			name: "TestPublishMessageInvalidHost",
			args: args{
				channel: "tasfast",
				msg:     "test",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := PublishMessage(tt.args.channel, tt.args.host, tt.args.clientId, tt.args.port, tt.args.msg, tt.args.tenantID, tt.args.messageType, tt.args.action, tt.args.metadata); (err != nil) != tt.wantErr {
				t.Errorf("PublishMessage() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestConnectToKubeMQ(t *testing.T) {
	type args struct {
		host     string
		clientId string
		port     int
		ctx      context.Context
	}
	tests := []struct {
		name    string
		args    args
		want    *kubemq.Client
		wantErr bool
	}{
		{
			name: "TestConnectToKubeMQ",
			args: args{
				host:     "localhost",
				clientId: "test",
				port:     5555,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ConnectToKubeMQ(tt.args.host, tt.args.clientId, tt.args.port, tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("ConnectToKubeMQ() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && nil == got {
				t.Errorf("ConnectToKubeMQ() got = %v", got)
			}
		})
	}
}

func TestCreateAsyncMessage(t *testing.T) {
	type args struct {
		msg      interface{}
		metadata map[string]string
	}
	tests := []struct {
		name        string
		args        args
		wantPayload []byte
		wantTags    map[string]string
		wantErr     bool
	}{
		{
			name: "TestCreateAsyncMessageByteMessage",
			args: args{
				msg: []byte(`true`),
			},
			wantPayload: []byte(`true`),
			wantTags:    map[string]string{"x-tenant-id": "0"},
			wantErr:     false,
		},
		{
			name: "TestCreateAsyncMessage",
			args: args{
				msg: true,
			},
			wantPayload: []byte(`true`),
			wantTags:    map[string]string{"x-tenant-id": "0"},
			wantErr:     false,
		},
		{
			name: "TestCreateAsyncMessageInvalidInput",
			args: args{
				msg: func() {},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotPayload, gotTags, err := CreateAsyncMessage(tt.args.msg, tt.args.metadata)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateAsyncMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotPayload, tt.wantPayload) {
				t.Errorf("CreateAsyncMessage() gotPayload = %v, want %v", gotPayload, tt.wantPayload)
			}
			if !reflect.DeepEqual(gotTags, tt.wantTags) {
				t.Errorf("CreateAsyncMessage() gotTags = %v, want %v", gotTags, tt.wantTags)
			}
		})
	}
}

func TestPublishErrorMessage(t *testing.T) {
	type args struct {
		msg      *kubemq.QueueMessage
		msgError error
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "TestPublishErrorMessageInvalidError",
			args:    args{},
			wantErr: false,
		},
		{
			name: "TestPublishErrorMessage",
			args: args{
				msg: &kubemq.QueueMessage{
					QueueMessage: &pb.QueueMessage{},
				},
				msgError: errors.New("tst"),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := PublishErrorMessage(tt.args.msg, tt.args.msgError); (err != nil) != tt.wantErr {
				t.Errorf("PublishErrorMessage() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestGetTagFromQueueMessage(t *testing.T) {
	type args struct {
		msg     *kubemq.QueueMessage
		tagName string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "TestGetTagFromQueueMessageNilMsg",
			want: "",
		},
		{
			name: "TestGetTagFromQueueMessageTagNotFound",
			args: args{
				msg: &kubemq.QueueMessage{
					QueueMessage: &pb.QueueMessage{
						Tags: map[string]string{},
					},
				},
				tagName: "test",
			},
			want: "",
		},
		{
			name: "TestGetTagFromQueueMessageTag",
			args: args{
				msg: &kubemq.QueueMessage{
					QueueMessage: &pb.QueueMessage{
						Metadata: `{"test": "abc"}`,
					},
				},
				tagName: "test",
			},
			want: "abc",
		},
		{
			name: "TestGetTagFromQueueMessageTagInvalidJSON",
			args: args{
				msg: &kubemq.QueueMessage{
					QueueMessage: &pb.QueueMessage{
						Metadata: `{"tes`,
					},
				},
				tagName: "test",
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetTagFromQueueMessage(tt.args.msg, tt.args.tagName); got != tt.want {
				t.Errorf("GetTagFromQueueMessage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetKubeMQStatus(t *testing.T) {
	isSubscribing = true
	tests := []struct {
		name string
		want bool
	}{
		{
			name: "TestGetKubeMQStatus",
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetKubeMQStatus(); got != tt.want {
				t.Errorf("GetKubeMQStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSubscribe(t *testing.T) {
	type args struct {
		channelName        string
		host               string
		clientId           string
		port               int
		processMsgFunction func(event *kubemq.QueueMessage) error
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestSubscribe",
			args: args{
				channelName: "test",
				host:        "localhost",
				clientId:    "test",
				port:        5555,
				processMsgFunction: func(event *kubemq.QueueMessage) error {
					fmt.Print(event)
					return nil
				},
			},
		},
		{
			name: "TestSubscribeInvalidHost",
			args: args{
				channelName: "test",
				processMsgFunction: func(event *kubemq.QueueMessage) error {
					fmt.Print(event)
					return nil
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Subscribe(tt.args.channelName, tt.args.host, tt.args.clientId, tt.args.port, tt.args.processMsgFunction)
		})
	}
}

func TestCloseClient(t *testing.T) {
	type args struct {
		client *kubemq.Client
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestCloseClient",
			args: args{
				client: &kubemq.Client{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			CloseClient(tt.args.client)
		})
	}
}

func TestAddTagToMetadata(t *testing.T) {
	type args struct {
		msgMetadata string
		key         string
		value       string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "TestAddTagToMetadata",
			args: args{
				msgMetadata: "",
				key:         "tst",
				value:       "test",
			},
			want: `{"tst":"test"}`,
		},
		{
			name: "TestAddTagToMetadataWithMeta",
			args: args{
				msgMetadata: "{}",
				key:         "tst",
				value:       "test",
			},
			want: `{"tst":"test"}`,
		},
		{
			name: "TestAddTagToMetadataInvalidMeta",
			args: args{
				msgMetadata: "{",
				key:         "tst",
				value:       "test",
			},
			want: ``,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AddTagToMetadata(tt.args.msgMetadata, tt.args.key, tt.args.value); got != tt.want {
				t.Errorf("AddTagToMetadata() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetMetadataAsMap(t *testing.T) {
	type args struct {
		msg *kubemq.QueueMessage
	}
	tests := []struct {
		name string
		args args
		want map[string]string
	}{
		{
			name: "TestGetMetadataAsMapNilMsg",
			args: args{
				msg: nil,
			},
			want: nil,
		},
		{
			name: "TestGetMetadataAsMapInvalidJSON",
			args: args{
				msg: &kubemq.QueueMessage{
					QueueMessage: &pb.QueueMessage{
						Metadata: `{"tes`,
					},
				},
			},
			want: nil,
		},
		{
			name: "TestGetMetadataAsMap",
			args: args{
				msg: &kubemq.QueueMessage{
					QueueMessage: &pb.QueueMessage{
						Metadata: `{}`,
					},
				},
			},
			want: map[string]string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetMetadataAsMap(tt.args.msg); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetMetadataAsMap() = %v, want %v", got, tt.want)
			}
		})
	}
}
