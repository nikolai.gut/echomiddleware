module gitlab.com/nikolai.straessle/echomiddleware/v3

go 1.14

require (
	github.com/casbin/casbin v1.9.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/elastic/go-sysinfo v1.4.0 // indirect
	github.com/elastic/go-windows v1.0.1 // indirect
	github.com/getsentry/sentry-go v0.7.0
	github.com/go-test/deep v1.0.5
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/google/uuid v1.1.1
	github.com/jinzhu/gorm v1.9.16
	github.com/kubemq-io/kubemq-go v1.4.0
	github.com/kubemq-io/protobuf v1.1.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/labstack/gommon v0.3.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/procfs v0.2.0 // indirect
	github.com/selvatico/go-mocket v1.0.7
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.4.0
	go.elastic.co/apm v1.8.0
	go.elastic.co/apm/module/apmechov4 v1.5.0
	go.elastic.co/apm/module/apmgorm v1.5.0
	go.elastic.co/fastjson v1.1.0 // indirect
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
	golang.org/x/sys v0.0.0-20200922070232-aee5d888a860 // indirect
	howett.net/plist v0.0.0-20200419221736-3b63eb3a43b5 // indirect
)
