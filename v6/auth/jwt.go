package auth

import (
	"errors"
	"fmt"
	"github.com/casbin/casbin"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
	"time"
)

const (
	TenantIDContextKey = "tenant_id"
	UserContextKey     = "user"
	TenantHeader       = "X-Tenant-ID"
	AnonymousRole      = "anonymous"
	AdminRole          = "admin"
	JwtHeaderPrefix    = "Bearer"
	AlgorithmRS256     = "RS256"
)

type (
	// JWTConfig defines the config for JWT middleware.
	JWTConfig struct {
		// Signing key to validate token. Used as fallback if SigningKeys has length 0.
		// Required. This or SigningKeys.
		SigningKey interface{}

		// Claims are extendable claims data defining token content.
		// Optional. Default value Claim
		Claims jwt.Claims

		// Is anonymous access allowed
		AllowNoJWT bool
		// The default claim to use when anonymous access is granted
		// The role is set to AnonymousRole
		NoAuthToken *jwt.Token
	}
)

// Common Errors
var (
	ErrJWTMissing = echo.NewHTTPError(http.StatusBadRequest, "missing or malformed jwt")
	ErrJWTInvalid = echo.NewHTTPError(http.StatusUnauthorized, "invalid or expired jwt")
	ErrBadRequest = echo.NewHTTPError(http.StatusBadRequest, "invalid request")
)

var (
	// DefaultJWTConfig is the default JWT auth middleware config.
	DefaultJWTConfig = JWTConfig{
		Claims: &Claim{},
		NoAuthToken: jwt.NewWithClaims(jwt.SigningMethodRS256, &Claim{
			StandardClaims: jwt.StandardClaims{
				IssuedAt: time.Now().Unix(),
			},
			User: User{
				Role: AnonymousRole,
			},
		}),
	}
)

// JWTWithConfig returns a JWT auth middleware with config.
// See: `JWT()`.
// JWT returns a JSON Web Token (JWT) auth middleware.
//
// For valid token, it sets the user in context and calls next handler.
// For invalid token, it returns "401 - Unauthorized" error.
// For missing token, it returns "400 - Bad Request" error.
//
// See: https://jwt.io/introduction
// See `JWTConfig.tokenHeader`
func JWTWithConfig(config JWTConfig) echo.MiddlewareFunc {
	if config.SigningKey == nil {
		panic("echo: jwt middleware requires signing key")
	}
	if config.Claims == nil {
		config.Claims = DefaultJWTConfig.Claims
	}
	if config.NoAuthToken == nil {
		config.NoAuthToken = DefaultJWTConfig.NoAuthToken
	}

	keyFunc := func(t *jwt.Token) (interface{}, error) {
		// Check the signing method
		if t.Method.Alg() != AlgorithmRS256 {
			return nil, errors.New(fmt.Sprintf("unexpected jwt signing method=%v", t.Header["alg"]))
		}
		return config.SigningKey, nil
	}

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			auth, err := jwtFromHeader(c)
			if err != nil && !config.AllowNoJWT {
				return err
			}
			token := new(jwt.Token)
			t := reflect.ValueOf(config.Claims).Type().Elem()
			claims := reflect.New(t).Interface().(jwt.Claims)
			token, err = jwt.ParseWithClaims(auth, claims, keyFunc)
			if err == nil && token.Valid {
				// Store user information from token into context.
				c.Set(UserContextKey, token)
				return next(c)
			}
			if config.AllowNoJWT {
				c.Set(UserContextKey, config.NoAuthToken)
				return next(c)
			}
			return &echo.HTTPError{
				Code:     ErrJWTInvalid.Code,
				Message:  ErrJWTInvalid.Message,
				Internal: err,
			}
		}
	}
}

func jwtFromHeader(c echo.Context) (string, error) {
	auth := c.Request().Header.Get(echo.HeaderAuthorization)
	l := len(JwtHeaderPrefix)
	if len(auth) > l+1 && auth[:l] == JwtHeaderPrefix {
		return auth[l+1:], nil
	}
	return "", ErrJWTMissing
}

func CheckJwtAndTenant(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		tenantValue := context.Request().Header.Get(TenantHeader)
		if "" == tenantValue {
			return echo.NewHTTPError(http.StatusBadRequest, "tenant_id missing")
		}
		u64, err := strconv.ParseUint(tenantValue, 10, 32)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "invalid tenant_id")
		}
		tenantId := uint(u64)
		context.Set(TenantIDContextKey, tenantId)

		value := context.Get(UserContextKey)
		if value == nil {
			return ErrJWTInvalid
		}
		token := value.(*jwt.Token)
		user := token.Claims.(*Claim)

		if areTenantsInvalid(tenantId, user) {
			return echo.NewHTTPError(http.StatusBadRequest, "tenant id from http-header and jwt not equal")
		}
		return next(context)
	}
}

func RegisterAuth(pathToPublicKey, model, policy, apiKey string, group *echo.Group) error {
	file, err := ioutil.ReadFile(pathToPublicKey)
	if err != nil {
		return err
	}
	key, err := jwt.ParseRSAPublicKeyFromPEM(file)
	if err != nil {
		return err
	}
	config := JWTConfig{
		SigningKey: key,
		AllowNoJWT: true,
	}
	group.Use(JWTWithConfig(config))
	group.Use(CheckJwtAndTenant)

	enforcer := Enforcer{
		Enforcer: casbin.NewEnforcer(model, policy),
	}
	group.Use(enforcer.Enforce)

	group.Use(middleware.KeyAuth(KeyFunc(apiKey)))

	return nil
}

func KeyFunc(apiKey string) func(key string, c echo.Context) (bool, error) {
	return func(key string, c echo.Context) (bool, error) {
		user, err := GetClaim(c)
		if err != nil {
			return false, err
		}
		if user.User.Role == AnonymousRole {
			return key == apiKey, nil
		}
		return true, nil
	}
}

func areTenantsInvalid(headerTenantId uint, user *Claim) bool {
	// anonymous doesnt have a jwt.tenant_id
	if user.User.Role == AnonymousRole {
		// anonymous cant have x-tenant-id to be 0
		return headerTenantId == uint(0)
	}
	// an admin can have every x-tenant-id
	if user.User.Role == AdminRole {
		return false
	}
	// a user can only have x-tenant-id to equal jwt.tenant_id
	return headerTenantId != user.User.TenantID
}

func GetClaim(context echo.Context) (*Claim, error) {
	value := context.Get(UserContextKey)
	if value == nil {
		return nil, ErrJWTInvalid
	}
	token := value.(*jwt.Token)
	user := token.Claims.(*Claim)
	return user, nil
}

func GetTenant(context echo.Context) (uint, error) {
	value := context.Get(TenantIDContextKey)
	if value == nil {
		return 0, ErrJWTInvalid
	}
	u64 := value.(uint)
	return u64, nil
}
