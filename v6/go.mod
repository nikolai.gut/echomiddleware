module gitlab.com/nikolai.straessle/echomiddleware/v6

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/casbin/casbin v1.9.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/getsentry/sentry-go v0.10.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/uuid v1.2.0
	github.com/kubemq-io/kubemq-go v1.4.7
	github.com/kubemq-io/protobuf v1.1.0
	github.com/labstack/echo/v4 v4.2.1
	github.com/nikolaistraessle/gorm-logrus v0.4.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/nikolai.straessle/gotestutils v1.4.2
	go.elastic.co/apm/module/apmgormv2 v1.11.0
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.5
)
