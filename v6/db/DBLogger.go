package db

import (
	"context"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm/logger"
	"time"
)

type Logger struct{}

func (l Logger) LogMode(level logger.LogLevel) logger.Interface {
	return l
}

func (l Logger) Info(ctx context.Context, s string, i ...interface{}) {
	logrus.WithContext(ctx).WithFields(l.CreateLogrusFields(i)).Info(s)
}

func (l Logger) Warn(ctx context.Context, s string, i ...interface{}) {
	logrus.WithContext(ctx).WithFields(l.CreateLogrusFields(i)).Warn(s)
}

func (l Logger) Error(ctx context.Context, s string, i ...interface{}) {
	logrus.WithContext(ctx).WithFields(l.CreateLogrusFields(i)).Error(s)
}

func (l Logger) Trace(ctx context.Context, begin time.Time, fc func() (string, int64), err error) {
	// not implemented
}

func (l Logger) CreateLogrusFields(data ...interface{}) logrus.Fields {
	return logrus.Fields{
		"module": "gorm",
		"data":   data,
	}
}
