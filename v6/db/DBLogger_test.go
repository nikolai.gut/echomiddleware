package db

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm/logger"
	"reflect"
	"testing"
	"time"
)

func TestLogger_LogMode(t *testing.T) {
	type args struct {
		level logger.LogLevel
	}
	tests := []struct {
		name string
		args args
		want logger.Interface
	}{
		{
			name: "TestLogger_LogMode",
			args: args{},
			want: Logger{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logger{}
			if got := l.LogMode(tt.args.level); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LogMode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLogger_Info(t *testing.T) {
	type args struct {
		ctx context.Context
		s   string
		i   []interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestLogger_Info",
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logger{}
			l.Info(context.Background(), "test")
			assert.True(t, true)
		})
	}
}
func TestLogger_Warn(t *testing.T) {
	type args struct {
		ctx context.Context
		s   string
		i   []interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestLogger_Warn",
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logger{}
			l.Warn(context.Background(), "test")
			assert.True(t, true)
		})
	}
}
func TestLogger_Error(t *testing.T) {
	type args struct {
		ctx context.Context
		s   string
		i   []interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestLogger_Error",
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logger{}
			l.Error(context.Background(), "test")
			assert.True(t, true)
		})
	}
}
func TestLogger_Trace(t *testing.T) {
	type args struct {
		ctx context.Context
		s   string
		i   []interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestLogger_Trace",
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logger{}
			l.Trace(context.Background(), time.Time{}, func() (string, int64) { return "", 0 }, nil)
			assert.True(t, true)
		})
	}
}

func TestLogger_CreateLogrusFields(t *testing.T) {
	type args struct {
		data []interface{}
	}
	tests := []struct {
		name string
		args args
		want logrus.Fields
	}{
		{
			name: "TestLogger_CreateLogrusFields",
			args: args{
				data: []interface{}{
					"test",
				},
			},
			want: logrus.Fields{
				"module": "gorm",
				"data": []interface{}{
					"test",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logger{}
			if got := l.CreateLogrusFields(tt.args.data...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateLogrusFields() = %v, want %v", got, tt.want)
			}
		})
	}
}
