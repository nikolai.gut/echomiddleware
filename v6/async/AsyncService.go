package async

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/kubemq-io/kubemq-go"
	"github.com/sirupsen/logrus"
	"os"
	"reflect"
	"strings"
)

const errorChannelName = "errors"

var testRun bool

var isSubscribing = false

func SetTestRun() {
	testRun = true
}

func ConnectToKubeMQ(host, clientId string, port int, ctx context.Context) (*kubemq.Client, error) {
	if nil == ctx {
		ctx = context.Background()
	}
	client, err := kubemq.NewClient(ctx,
		kubemq.WithAddress(host, port),
		kubemq.WithClientId(fmt.Sprintf("%s-%s", clientId, uuid.New().String())),
		kubemq.WithTransportType(kubemq.TransportTypeGRPC))
	if nil != err {
		return nil, err
	}
	return client, nil
}

func Subscribe(channelName, host, clientId string, port int, processMsgFunction func(event *kubemq.QueueMessage) error) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	client, err := ConnectToKubeMQ(host, clientId, port, ctx)
	if nil != err {
		logrus.WithFields(logrus.Fields{
			"host":      host,
			"port":      port,
			"client_id": clientId,
		}).Error(err)
		return
	}
	defer CloseClient(client)
	isSubscribing = true
	defer func() {
		logrus.WithFields(logrus.Fields{
			"host":      host,
			"port":      port,
			"client_id": clientId,
		}).Debug("stop subscribing messages")
		isSubscribing = false
	}()
	for {
		receiveResult, err := client.NewReceiveQueueMessagesRequest().
			SetChannel(channelName).
			SetMaxNumberOfMessages(10).
			SetWaitTimeSeconds(5).
			Send(ctx)
		if nil != err && !strings.HasPrefix(err.Error(), "Error 138") {
			logrus.WithFields(logrus.Fields{
				"host":      host,
				"port":      port,
				"client_id": clientId,
			}).Error(err)
			return
		} else if 0 == receiveResult.MessagesReceived && !testRun {
			continue
		} else if 0 == receiveResult.MessagesReceived && testRun {
			return
		}
		for _, msg := range receiveResult.Messages {
			logrus.WithFields(logrus.Fields{
				"host":      host,
				"port":      port,
				"client_id": clientId,
				"msg_id":    msg.MessageID,
				"channel":   msg.Channel,
				"tags":      msg.Tags,
				"metadata":  msg.Metadata,
			}).Debug("received message")
			if err := processMsgFunction(msg); nil != err {
				if err := PublishErrorMessage(msg, err); nil != err {
					logrus.WithFields(logrus.Fields{
						"client_id": msg.ClientID,
						"msg_id":    msg.MessageID,
						"channel":   msg.Channel,
						"tags":      msg.Tags,
						"metadata":  msg.Metadata,
						"error":     err,
					}).Debug("failed to publish error message")
				}
			}
		}
	}
}

func CloseClient(client *kubemq.Client) {
	if nil != client {
		err := client.Close()
		if nil != err {
			logrus.Error(err)
		}
	}
}

func GetMetadataAsMap(msg *kubemq.QueueMessage) map[string]string {
	var metadata map[string]string
	if nil == msg || "" == msg.Metadata {
		return metadata
	}
	err := json.Unmarshal([]byte(msg.Metadata), &metadata)
	if nil != err {
		logrus.WithFields(logrus.Fields{
			"client_id": msg.ClientID,
			"msg_id":    msg.MessageID,
			"channel":   msg.Channel,
			"tags":      msg.Tags,
			"metadata":  msg.Metadata,
			"error":     err,
		}).Debug("failed to unmarshal metadata")
	}
	return metadata
}

func GetTagFromQueueMessage(msg *kubemq.QueueMessage, tagName string) string {
	if nil == msg {
		return ""
	}
	if "" == msg.Metadata {
		return ""
	}
	var metadata map[string]string
	err := json.Unmarshal([]byte(msg.Metadata), &metadata)
	if nil != err {
		logrus.WithFields(logrus.Fields{
			"client_id": msg.ClientID,
			"msg_id":    msg.MessageID,
			"channel":   msg.Channel,
			"tags":      msg.Tags,
			"metadata":  msg.Metadata,
			"error":     err,
		}).Debug("failed to unmarshal metadata")
		return ""
	}
	tag, ok := metadata[tagName]
	if !ok {
		return ""
	}
	return tag
}

func AddTagToMetadata(msgMetadata, key, value string) string {
	var metadata map[string]string
	if "" != msgMetadata {
		err := json.Unmarshal([]byte(msgMetadata), &metadata)
		if nil != err {
			logrus.WithFields(logrus.Fields{
				"error": err,
			}).Debug("failed to unmarshal metadata")
			return ""
		}
	} else {
		metadata = make(map[string]string, 1)
	}
	metadata[key] = value
	metaStringBytes, err := json.Marshal(&metadata)
	if nil != err {
		logrus.WithFields(logrus.Fields{
			"error": err,
		}).Debug("failed to unmarshal metadata")
		return ""
	}
	return string(metaStringBytes)
}

func PublishErrorMessage(msg *kubemq.QueueMessage, msgError error) error {
	if nil == msg || nil == msgError {
		return nil
	}
	logrus.WithFields(logrus.Fields{
		"client_id": msg.ClientID,
		"msg_id":    msg.MessageID,
		"channel":   msg.Channel,
		"tags":      msg.Tags,
		"metadata":  msg.Metadata,
	}).Debug("publish error message")
	msg.Metadata = AddTagToMetadata(msg.Metadata, "error", msgError.Error())
	msg.Metadata = AddTagToMetadata(msg.Metadata, "error_app", os.Getenv("APP_NAME"))
	msg.SetChannel(errorChannelName)
	_, err := msg.Send(context.Background())
	return err
}

//PublishMessage publishes a message to a given topic
func PublishMessage(channel, host, clientId string, port int, msg interface{}, tenantID uint, messageType, action string, metadata map[string]string) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	client, err := ConnectToKubeMQ(host, clientId, port, ctx)
	if nil != err {
		return err
	}
	defer CloseClient(client)
	queueMsg := client.NewQueueMessage().
		SetChannel(channel)
	if nil == metadata {
		metadata = map[string]string{}
	}
	metadata["x-tenant-id"] = fmt.Sprintf("%d", tenantID)
	metadata["type"] = messageType
	metadata["action"] = action
	hasHeritage := true
	heritageNumber := 0
	for hasHeritage {
		if _, ok := metadata[fmt.Sprintf("heritage_%d", heritageNumber)]; ok {
			heritageNumber++
			continue
		}
		hasHeritage = false
		metadata[fmt.Sprintf("heritage_%d", heritageNumber)] = os.Getenv("APP_NAME")
	}
	hasTarget := true
	targetNumber := 0
	for hasTarget {
		if _, ok := metadata[fmt.Sprintf("target_%d", targetNumber)]; ok {
			targetNumber++
			continue
		}
		hasTarget = false
		metadata[fmt.Sprintf("target_%d", targetNumber)] = channel
	}
	// create an async message from given msg data and metadata
	queueMsg.Body, metadata, err = CreateAsyncMessage(msg, metadata, tenantID)
	for k, v := range metadata {
		queueMsg.Metadata = AddTagToMetadata(queueMsg.Metadata, k, v)
	}
	if nil != err {
		return err
	}
	if "" == queueMsg.MessageID {
		queueMsg.MessageID = uuid.New().String()
	}
	logrus.WithFields(logrus.Fields{
		"client_id": queueMsg.ClientID,
		"msg_id":    queueMsg.MessageID,
		"channel":   queueMsg.Channel,
		"tags":      queueMsg.Tags,
		"metadata":  queueMsg.Metadata,
	}).Info("publish msg")
	_, err = queueMsg.Send(ctx)
	return err
}

//CreateAsyncMessage creates an amqp message from given msg data and metadata
func CreateAsyncMessage(msg interface{}, metadata map[string]string, tenant uint) (payload []byte, tags map[string]string, err error) {
	return CreateAsyncMessageWithTenantId(msg, metadata, tenant)
}

//CreateAsyncMessage creates an amqp message from given msg data and metadata
func CreateAsyncMessageWithTenantId(msg interface{}, metadata map[string]string, tenantId uint) (payload []byte, tags map[string]string, err error) {
	if reflect.TypeOf([]byte("")) != reflect.TypeOf(msg) {
		// create json from given msg data
		payload, err = json.Marshal(msg)
		if nil != err {
			return nil, nil, err
		}
	} else {
		payload = msg.([]byte)
	}
	if nil == metadata {
		metadata = map[string]string{}
	}
	_, ok := metadata["x-tenant-id"]
	if 0 != tenantId || !ok {
		// add x-tenant-id header
		metadata["x-tenant-id"] = fmt.Sprintf("%d", tenantId)
	}
	return payload, metadata, nil
}

func GetKubeMQStatus() bool {
	return isSubscribing
}
