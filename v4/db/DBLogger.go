package db

import (
	"context"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm/logger"
	"time"
)

type DBLogger struct{}

func (l DBLogger) LogMode(level logger.LogLevel) logger.Interface {
	return l
}

func (l DBLogger) Info(ctx context.Context, s string, i ...interface{}) {
	logrus.WithFields(l.CreateLogrusFields(i)).Info(s)
}

func (l DBLogger) Warn(ctx context.Context, s string, i ...interface{}) {
	logrus.WithFields(l.CreateLogrusFields(i)).Warn(s)
}

func (l DBLogger) Error(ctx context.Context, s string, i ...interface{}) {
	logrus.WithFields(l.CreateLogrusFields(i)).Error(s)
}

func (l DBLogger) Trace(ctx context.Context, begin time.Time, fc func() (string, int64), err error) {
	// not implemented
}

func (l DBLogger) CreateLogrusFields(data ...interface{}) logrus.Fields {
	return logrus.Fields{
		"module": "gorm",
		"data":   data,
	}
}
