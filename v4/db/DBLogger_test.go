package db

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm/logger"
	"reflect"
	"testing"
	"time"
)

func TestDBLogger_LogMode(t *testing.T) {
	type args struct {
		level logger.LogLevel
	}
	tests := []struct {
		name string
		args args
		want logger.Interface
	}{
		{
			name: "TestDBLogger_LogMode",
			args: args{},
			want: DBLogger{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := DBLogger{}
			if got := l.LogMode(tt.args.level); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LogMode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDBLogger_Info(t *testing.T) {
	type args struct {
		ctx context.Context
		s   string
		i   []interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestDBLogger_Info",
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := DBLogger{}
			l.Info(context.Background(), "test")
			assert.True(t, true)
		})
	}
}
func TestDBLogger_Warn(t *testing.T) {
	type args struct {
		ctx context.Context
		s   string
		i   []interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestDBLogger_Warn",
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := DBLogger{}
			l.Warn(context.Background(), "test")
			assert.True(t, true)
		})
	}
}
func TestDBLogger_Error(t *testing.T) {
	type args struct {
		ctx context.Context
		s   string
		i   []interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestDBLogger_Error",
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := DBLogger{}
			l.Error(context.Background(), "test")
			assert.True(t, true)
		})
	}
}
func TestDBLogger_Trace(t *testing.T) {
	type args struct {
		ctx context.Context
		s   string
		i   []interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestDBLogger_Trace",
			args: args{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := DBLogger{}
			l.Trace(context.Background(), time.Time{}, func() (string, int64) { return "", 0 }, nil)
			assert.True(t, true)
		})
	}
}

func TestDBLogger_CreateLogrusFields(t *testing.T) {
	type args struct {
		data []interface{}
	}
	tests := []struct {
		name string
		args args
		want logrus.Fields
	}{
		{
			name: "TestDBLogger_CreateLogrusFields",
			args: args{
				data: []interface{}{
					"test",
				},
			},
			want: logrus.Fields{
				"module": "gorm",
				"data": []interface{}{
					"test",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := DBLogger{}
			if got := l.CreateLogrusFields(tt.args.data...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateLogrusFields() = %v, want %v", got, tt.want)
			}
		})
	}
}
