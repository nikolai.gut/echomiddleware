package user

import (
	"gorm.io/gorm"
	"time"
)

var CurrentUser User

type User struct {
	gorm.Model
	Username           string     `json:"username"`
	Password           string     `json:"password"`
	FirstName          string     `json:"first_name"`
	LastName           string     `json:"last_name"`
	Email              string     `json:"email"`
	Phone              string     `json:"phone"`
	Role               string     `json:"role"`
	LastPasswordChange *time.Time `json:"last_password_change"`
	TWAMethod          string     `json:"twa_method"`
	SchoolMember       bool       `json:"school_member"`
	TOTPSecret         string     `json:"-"`
	HOTPSecret         string     `json:"-"`
	HOTPCounter        uint64     `json:"-"`
	JWT                string     `json:"jwt" gorm:"-" sql:"-"`
	CompanyID          uint       `json:"company_id"`
	TenantID           uint       `json:"tenant_id"`
}
