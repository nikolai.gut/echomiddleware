/*
 * Copyright (c) 2019 Nikolai Strässle.  All rights reserved.
 */

package db

import (
	"context"
	"fmt"
	"github.com/jinzhu/gorm"
	mocket "github.com/selvatico/go-mocket"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikolai.straessle/echomiddleware"
	_ "go.elastic.co/apm/module/apmgorm/dialects/mysql"
	_ "go.elastic.co/apm/module/apmgorm/dialects/postgres"
	_ "go.elastic.co/apm/module/apmgorm/dialects/sqlite"
	"strings"
	"testing"
)

func createDatabaseConnection() *gorm.DB { // or *gorm.DB
	mocket.Catcher.Register() // Safe register. Allowed multiple calls to save
	mocket.Catcher.Logging = true
	// GORM
	db, err := gorm.Open(mocket.DriverName, "connection_string")
	if nil != err {
		fmt.Print(err)
	}
	db.LogMode(true)
	return db
}

func init() {
	db := createDatabaseConnection()
	DB = Database{
		Database: func() (db *gorm.DB, e error) {
			if nil == DB.DatabaseGorm || nil == DB.DatabaseGorm.DB() {
				db = createDatabaseConnection()
			} else if err := DB.DatabaseGorm.DB().Ping(); nil != err {
				db = createDatabaseConnection()
			}
			DB.DatabaseGorm = db
			return DB.DatabaseGorm, nil
		},
		DatabaseGorm: db,
		Context:      context.TODO(),
	}
}

func SetupTests() *gorm.DB { // or *gorm.DB
	mocket.Catcher.Register() // Safe register. Allowed multiple calls to save
	mocket.Catcher.Logging = true
	// GORM
	db, err := gorm.Open(mocket.DriverName, "connection_string")
	if nil != err {
		fmt.Print(err)
	}

	return db
}

func TestDatabase_Init(t *testing.T) {
	db1 := SetupTests()
	type fields struct {
		Database     func() (*gorm.DB, error)
		DatabaseGorm *gorm.DB
		DriverName   string
		Username     string
		Password     string
		Host         string
		Port         int
		DatabaseName string
		DatabaseFile string
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "TestDatabaseInit",
			fields: fields{
				Database: func() (db *gorm.DB, e error) {
					return db1, nil
				},
				DatabaseGorm: db1,
				DriverName:   "mock",
				Username:     "a",
				Password:     "a",
				Host:         "h",
				Port:         1,
				DatabaseName: "a",
				DatabaseFile: "a",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &Database{
				Database:     tt.fields.Database,
				DatabaseGorm: tt.fields.DatabaseGorm,
				DriverName:   tt.fields.DriverName,
				Username:     tt.fields.Username,
				Password:     tt.fields.Password,
				Host:         tt.fields.Host,
				Port:         tt.fields.Port,
				DatabaseName: tt.fields.DatabaseName,
				DatabaseFile: tt.fields.DatabaseFile,
			}
			db.Init()
		})
	}
}

func TestDatabase_DatabaseFunction(t *testing.T) {
	db1 := SetupTests()
	db2 := SetupTests()
	db2.Close()
	type fields struct {
		Database     func() (*gorm.DB, error)
		DatabaseGorm *gorm.DB
		DriverName   string
		Username     string
		Password     string
		Host         string
		Port         int
		DatabaseName string
		DatabaseFile string
		Context      context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "TestDatabase_DatabaseFunction",
			fields: fields{
				Database: func() (db *gorm.DB, e error) {
					return db1, nil
				},
				DatabaseGorm: db1,
				DriverName:   "mock",
				Username:     "a",
				Password:     "a",
				Host:         "h",
				Port:         1,
				DatabaseName: "a",
				DatabaseFile: "a",
			},
			wantErr: false,
		},
		{
			name: "TestDatabase_DatabaseFunction",
			fields: fields{
				Database: func() (db *gorm.DB, e error) {
					return db1, nil
				},
				DatabaseGorm: db1,
				DriverName:   "mock",
				Username:     "a",
				Password:     "a",
				Host:         "h",
				Port:         1,
				DatabaseName: "a",
				DatabaseFile: "a",
				Context:      context.TODO(),
			},
			wantErr: false,
		},
		{
			name: "TestDatabase_DatabaseFunctionClosedConnection",
			fields: fields{
				Database: func() (db *gorm.DB, e error) {
					return db1, nil
				},
				DatabaseGorm: db2,
				DriverName:   "mock",
				Username:     "a",
				Password:     "a",
				Host:         "h",
				Port:         1,
				DatabaseName: "a",
				DatabaseFile: "a",
			},
			wantErr: true,
		},
		{
			name: "TestDatabase_DatabaseFunctionClosedConnection",
			fields: fields{
				Database: func() (db *gorm.DB, e error) {
					return db1, nil
				},
				DatabaseGorm: nil,
				DriverName:   "mysql",
				DatabaseName: "test",
				Host:         "test",
				Port:         1,
				Username:     "test",
				Password:     "test",
			},
			wantErr: true,
		},
		{
			name: "TestDatabase_DatabaseFunctionClosedConnection",
			fields: fields{
				Database: func() (db *gorm.DB, e error) {
					return db1, nil
				},
				DatabaseGorm: nil,
				DriverName:   "mssql",
				DatabaseName: "test",
				Host:         "test",
				Port:         1,
				Username:     "test",
				Password:     "test",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &Database{
				Database:     tt.fields.Database,
				DatabaseGorm: tt.fields.DatabaseGorm,
				DriverName:   tt.fields.DriverName,
				Username:     tt.fields.Username,
				Password:     tt.fields.Password,
				Host:         tt.fields.Host,
				Port:         tt.fields.Port,
				DatabaseName: tt.fields.DatabaseName,
				DatabaseFile: tt.fields.DatabaseFile,
				Context:      tt.fields.Context,
			}
			db.Init()
			_, err := db.Database()
			if (err != nil) != tt.wantErr {
				t.Errorf("Database.CreateDatabaseConnectionString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestDatabase_CreateDatabaseConnectionString(t *testing.T) {
	type fields struct {
		Database     func() (*gorm.DB, error)
		DatabaseGorm *gorm.DB
		DriverName   string
		Username     string
		Password     string
		Host         string
		Port         int
		DatabaseName string
		DatabaseFile string
	}
	sqliteConnectionString := "db.db"
	tests := []struct {
		name                 string
		fields               fields
		wantConnectionString string
		wantErr              bool
	}{
		{
			name: "TestCreateDatabaseConnectionStringSqlite",
			fields: fields{
				DriverName:   "sqlite3",
				DatabaseFile: sqliteConnectionString,
			},
			wantConnectionString: "",
			wantErr:              true,
		},
		{
			name: "TestCreateDatabaseConnectionStringSqliteNoFilePath",
			fields: fields{
				DriverName: "sqlite3",
			},
			wantConnectionString: "",
			wantErr:              true,
		},
		{
			name: "TestCreateDatabaseConnectionStringMySQL",
			fields: fields{
				DriverName:   "mysql",
				DatabaseName: "test",
				Host:         "test",
				Port:         1,
				Username:     "test",
				Password:     "test",
			},
			wantConnectionString: fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", "test", "test", "test", 1, "test"),
			wantErr:              false,
		},
		{
			name: "TestCreateDatabaseConnectionStringMySQLNoDatabaseName",
			fields: fields{
				DriverName: "mysql",
			},
			wantConnectionString: "",
			wantErr:              true,
		},
		{
			name: "TestCreateDatabaseConnectionStringMySQLNoHostNoPort",
			fields: fields{
				DriverName:   "mysql",
				DatabaseName: "test",
			},
			wantConnectionString: fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", "", "", "localhost", 3306, "test"),
			wantErr:              false,
		},
		{
			name: "TestCreateDatabaseConnectionStringPG",
			fields: fields{
				DriverName:   "postgres",
				DatabaseName: "test",
				Host:         "test",
				Port:         1,
				Username:     "test",
				Password:     "test",
			},
			wantConnectionString: fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s", "test", 1, "test", "test", "test"),
			wantErr:              false,
		},
		{
			name: "TestCreateDatabaseConnectionStringPGNoDatabaseName",
			fields: fields{
				DriverName: "postgres",
			},
			wantConnectionString: "",
			wantErr:              true,
		},
		{
			name: "TestCreateDatabaseConnectionStringPGNoHostNoPort",
			fields: fields{
				DriverName:   "postgres",
				DatabaseName: "test",
			},
			wantConnectionString: fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s", "localhost", 5432, "", "test", ""),
			wantErr:              false,
		},
		{
			name: "TestCreateDatabaseConnectionStringMSSQL",
			fields: fields{
				DriverName:   "mssql",
				DatabaseName: "test",
				Host:         "test",
				Port:         1,
				Username:     "test",
				Password:     "test",
			},
			wantConnectionString: fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s", "test", "test", "test", 1, "test"),
			wantErr:              false,
		},
		{
			name: "TestCreateDatabaseConnectionStringMSSQLNoDatabaseName",
			fields: fields{
				DriverName: "mssql",
			},
			wantConnectionString: "",
			wantErr:              true,
		},
		{
			name: "TestCreateDatabaseConnectionStringMSSQLNoHostNoPort",
			fields: fields{
				DriverName:   "mssql",
				DatabaseName: "test",
			},
			wantConnectionString: fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s", "", "", "localhost", 1433, "test"),
			wantErr:              false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &Database{
				Database:     tt.fields.Database,
				DatabaseGorm: tt.fields.DatabaseGorm,
				DriverName:   tt.fields.DriverName,
				Username:     tt.fields.Username,
				Password:     tt.fields.Password,
				Host:         tt.fields.Host,
				Port:         tt.fields.Port,
				DatabaseName: tt.fields.DatabaseName,
				DatabaseFile: tt.fields.DatabaseFile,
			}
			gotConnectionString, err := db.CreateDatabaseConnectionString()
			if (err != nil) != tt.wantErr {
				t.Errorf("Database.CreateDatabaseConnectionString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotConnectionString != tt.wantConnectionString {
				t.Errorf("Database.CreateDatabaseConnectionString() = %v, want %v", gotConnectionString, tt.wantConnectionString)
			}
		})
	}
}

func TestDatabase_CloseDBAndHandleError(t *testing.T) {
	db1 := SetupTests()
	db1.Close()
	type fields struct {
		Database     func() (*gorm.DB, error)
		DatabaseGorm *gorm.DB
		DriverName   string
		Username     string
		Password     string
		Host         string
		Port         int
		DatabaseName string
		DatabaseFile string
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "TestCloseDBAndHandleError",
			fields: fields{
				Database: func() (db *gorm.DB, e error) {
					return nil, nil
				},
				DatabaseGorm: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &Database{
				Database:     tt.fields.Database,
				DatabaseGorm: tt.fields.DatabaseGorm,
				DriverName:   tt.fields.DriverName,
				Username:     tt.fields.Username,
				Password:     tt.fields.Password,
				Host:         tt.fields.Host,
				Port:         tt.fields.Port,
				DatabaseName: tt.fields.DatabaseName,
				DatabaseFile: tt.fields.DatabaseFile,
			}
			db.CloseDBAndHandleError()
		})
	}
}

func TestDatabase_Migrate(t *testing.T) {
	db1 := SetupTests()
	type fields struct {
		Database     func() (*gorm.DB, error)
		DatabaseGorm *gorm.DB
		DriverName   string
		Username     string
		Password     string
		Host         string
		Port         int
		DatabaseName string
		DatabaseFile string
	}
	type args struct {
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "TestDatabaseMigrate",
			fields: fields{
				Database: func() (db *gorm.DB, e error) {
					return db1, nil
				},
				DatabaseGorm: db1,
			},
			args:    args{},
			wantErr: false,
		},
		{
			name: "TestDatabaseMigrateNoDatabaseGorm",
			fields: fields{
				Database: func() (db *gorm.DB, e error) {
					return db1, nil
				},
				DatabaseGorm: nil,
			},
			args:    args{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &Database{
				Database:     tt.fields.Database,
				DatabaseGorm: tt.fields.DatabaseGorm,
				DriverName:   tt.fields.DriverName,
				Username:     tt.fields.Username,
				Password:     tt.fields.Password,
				Host:         tt.fields.Host,
				Port:         tt.fields.Port,
				DatabaseName: tt.fields.DatabaseName,
				DatabaseFile: tt.fields.DatabaseFile,
			}
			if err := db.Migrate(); (err != nil) != tt.wantErr {
				t.Errorf("Database.Migrate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDatabase_SetContext(t *testing.T) {
	type fields struct {
		Database     func() (*gorm.DB, error)
		DatabaseGorm *gorm.DB
		DriverName   string
		Username     string
		Password     string
		Host         string
		Port         int
		DatabaseName string
		DatabaseFile string
		Context      context.Context
	}
	type args struct {
		ctx      context.Context
		callback func()
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "TestDatabase_SetContext",
			fields: fields{
				Database:     nil,
				DatabaseGorm: nil,
				DriverName:   "",
				Username:     "",
				Password:     "",
				Host:         "",
				Port:         0,
				DatabaseName: "",
				DatabaseFile: "",
				Context:      nil,
			},
			args: args{
				ctx: context.TODO(),
				callback: func() {

				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &Database{
				Database:     tt.fields.Database,
				DatabaseGorm: tt.fields.DatabaseGorm,
				DriverName:   tt.fields.DriverName,
				Username:     tt.fields.Username,
				Password:     tt.fields.Password,
				Host:         tt.fields.Host,
				Port:         tt.fields.Port,
				DatabaseName: tt.fields.DatabaseName,
				DatabaseFile: tt.fields.DatabaseFile,
				Context:      tt.fields.Context,
			}
			db.SetContext(tt.args.ctx, tt.args.callback)
		})
	}
}

func TestCreateDBQuery(t *testing.T) {
	type args struct {
		tenantId uint
		model    interface{}
		conn     string
	}
	tests := []struct {
		name string
		args args
		want *gorm.DB
	}{
		{
			name: "TestCreateDBQuery",
			args: args{
				model: &args{},
			},
			want: nil,
		},
		{
			name: "TestCreateDBQueryWithTenantId",
			args: args{
				tenantId: 1,
			},
			want: nil,
		},
		{
			name: "TestCreateDBQuery",
			args: args{
				model: &args{},
				conn:  "close",
			},
			want: nil,
		},
		{
			name: "TestCreateDBQuery",
			args: args{
				model: &args{},
				conn:  "nil",
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			echomiddleware.TenantId = tt.args.tenantId
			switch tt.args.conn {
			case "close":
				DB.DatabaseGorm.Close()
			case "nil":
				DB.DatabaseGorm = nil
			}
			got := CreateDBQuery(tt.args.model)
			expr := got.QueryExpr()
			if 0 != tt.args.tenantId && !strings.Contains(fmt.Sprint(expr), "tenant_id") {
				t.Errorf("CreateDBQuery() = %v, missing tenant_id", expr)
			}
		})
	}
}

func TestRollbacTransactionkUnlessCommitted(t *testing.T) {
	DB.Transaction = DB.DatabaseGorm.Begin()
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name:    "TestRollbacTransactionkUnlessCommitted",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			DB.Transaction.Save(&struct {
				Test string
			}{})
			if err := RollbacTransactionkUnlessCommitted(); (err != nil) != tt.wantErr {
				t.Errorf("RollbacTransactionkUnlessCommitted() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCommitTransaction(t *testing.T) {
	DB.Transaction = DB.DatabaseGorm.Begin()
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name:    "TestCommitTransaction",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := CommitTransaction(); (err != nil) != tt.wantErr {
				t.Errorf("CommitTransaction() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestBeginTransaction(t *testing.T) {
	tests := []struct {
		name string
		conn string
	}{
		{
			name: "TestBeginTransaction",
		},
		{
			name: "TestBeginTransactionClose",
			conn: "close",
		},
		{
			name: "TestBeginTransactionNil",
			conn: "nil",
		},
	}
	for _, tt := range tests {
		switch tt.conn {
		case "close":
			DB.DatabaseGorm.Close()
		case "nil":
			DB.DatabaseGorm = nil
		}
		t.Run(tt.name, func(t *testing.T) {
			assert.NotPanics(t, BeginTransaction, "should not panic")
		})
	}
}
