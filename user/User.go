package user

import (
	"github.com/jinzhu/gorm"
	"time"
)

var CurrentUser User

type User struct {
	gorm.Model
	Username           string     `json:"username"`
	Password           string     `json:"password"`
	FirstName          string     `json:"first_name"`
	LastName           string     `json:"last_name"`
	Email              string     `json:"email"`
	Phone              string     `json:"phone"`
	Role               string     `json:"role"`
	LastPasswordChange *time.Time `json:"last_password_change"`
	TWAMethod          string     `json:"twa_method"`
	TOTPSecret         string     `json:"-"`
	HOTPSecret         string     `json:"-"`
	HOTPCounter        uint64     `json:"-"`
	JWT                string     `json:"jwtmiddleware" gorm:"-" sql:"-"`
	TenantID           uint       `json:"tenant_id"`
}
