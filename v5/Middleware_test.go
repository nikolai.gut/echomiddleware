package echomiddleware

import (
	"errors"
	"fmt"
	"github.com/casbin/casbin"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-test/deep"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikolai.straessle/echomiddleware/v5/jwtmiddleware"
	"gitlab.com/nikolai.straessle/echomiddleware/v5/user"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

const JwtKeyHs256 = `eyJ0eXAiOiJKV1QiLCJraWQiOiIwMDAxIiwiaXNzIjoidGVzdCIsImFsZyI6IkhTMjU2IiwiaWF0IjoxNTU3NTU5NDMwLCJleHAiOjExNTU3NTU5NDMwfQ.eyJjb21wYW55Ijp7IklEIjoxLCJDcmVhdGVkQXQiOiIyMDE5LTA1LTA3VDA3OjE5OjI1WiIsIlVwZGF0ZWRBdCI6IjIwMTktMDUtMDdUMDc6MTk6MjVaIiwiRGVsZXRlZEF0IjpudWxsLCJuYW1lIjoiYXJjYWRlIHNvbHV0aW9ucyBhZyIsInR5cGUiOiJhZG1pbiIsImFkZHJlc3MiOiJXaW5rZWxyaWVkc3RyYXNzZSAzNyIsInppcCI6IjYwMDMiLCJjaXR5IjoiTHV6ZXJuIiwiY29udGFjdF9lbWFpbCI6Im5zdEBhcmNhZGUuY2giLCJjb250YWN0X3Bob25lIjoiMDc5OTM2ODQ1MyIsInVzZXJzIjpudWxsLCJwYXJlbnRfY29tcGFueV9pZCI6MH0sImV4cCI6MjU1NzMwNDkyNywidXNlciI6eyJJRCI6MSwiQ3JlYXRlZEF0IjoiMjAxOS0wNS0wN1QwNzoxOToyNVoiLCJVcGRhdGVkQXQiOiIyMDE5LTA1LTA3VDA3OjE5OjI1WiIsIkRlbGV0ZWRBdCI6bnVsbCwidXNlcm5hbWUiOiJhZG1pbiIsImZpcnN0bmFtZSI6Ik5pa29sYWkiLCJsYXN0bmFtZSI6IlN0csOkc3NsZSIsImVtYWlsIjoibnN0QGFyY2FkZS5jaCIsInJvbGUiOiJhZG1pbiIsInRvdHBfY29kZSI6IiIsImNvbXBhbnlfcmVmZXIiOjF9fQ.9NtjWj8G7fpd5RAawJ0DuKbFpe9Mpm7UvR1upyZUUcQ`
const JwtNoAdminKey = `eyJ0eXAiOiJKV1QiLCJraWQiOiIwMDAxIiwiaXNzIjoidGVzdCIsImFsZyI6IkhTMjU2IiwiaWF0IjoxNTU3NTU5MDQzLCJleHAiOjExNTU3NTU5MDQzfQ.eyJjb21wYW55Ijp7IklEIjoxLCJDcmVhdGVkQXQiOiIyMDE5LTA1LTA3VDA3OjE5OjI1WiIsIlVwZGF0ZWRBdCI6IjIwMTktMDUtMDdUMDc6MTk6MjVaIiwiRGVsZXRlZEF0IjpudWxsLCJuYW1lIjoiYXJjYWRlIHNvbHV0aW9ucyBhZyIsInR5cGUiOiJhZG1pbiIsImFkZHJlc3MiOiJXaW5rZWxyaWVkc3RyYXNzZSAzNyIsInppcCI6IjYwMDMiLCJjaXR5IjoiTHV6ZXJuIiwiY29udGFjdF9lbWFpbCI6Im5zdEBhcmNhZGUuY2giLCJjb250YWN0X3Bob25lIjoiMDc5OTM2ODQ1MyIsInVzZXJzIjpudWxsLCJwYXJlbnRfY29tcGFueV9pZCI6MH0sImV4cCI6MjU1NzMwNDkyNywidXNlciI6eyJJRCI6MSwiQ3JlYXRlZEF0IjoiMjAxOS0wNS0wN1QwNzoxOToyNVoiLCJVcGRhdGVkQXQiOiIyMDE5LTA1LTA3VDA3OjE5OjI1WiIsIkRlbGV0ZWRBdCI6bnVsbCwidXNlcm5hbWUiOiJhZG1pbiIsImZpcnN0bmFtZSI6Ik5pa29sYWkiLCJsYXN0bmFtZSI6IlN0csOkc3NsZSIsImVtYWlsIjoibnN0QGFyY2FkZS5jaCIsInJvbGUiOiJjaXZpbHN0YXR1c29mZmljZSIsInRvdHBfY29kZSI6IiIsImNvbXBhbnlfcmVmZXIiOjF9fQ.1WbafYG7pHLWpua0coPn_uW4k_jrA9v9FJZyUdEsMws`
const JwtNoUserKey = `eyJ0eXAiOiJKV1QiLCJraWQiOiIwMDAxIiwiaXNzIjoidGVzdCIsImFsZyI6IkhTMjU2IiwiaWF0IjoxNTU3NTU5NDMwLCJleHAiOjExNTU3NTU5NDMwfQ.eyJleHAiOjI1NTczMDQ5Mjd9.2mbyyNqA5XoYb_dNl2r3Oz0jE__cP3pKvuXRZX_akRw`
const JwtKeyHs256Invalid = `eyJ0eXAiOiJKV1QiLCJraWQiOiIwMDAxIiwiaXNzIjoidGVzdCIsImFsZyI6IkhTMjU2IiwiaWF0IjoxNTU3NTU5NDMwLCJleHAiOjExNTU3NTU5NDMwfQ.eyJjb21wYW55Ijp7IklEIjoxLCJDcmVhdGVkQXQiOiIyMDE5LTA1LTA3VDA3OjE5OjI1WiIsIlVwZGF0ZWRBdCI6IjIwMTktMDUtMDdUMDc6MTk6MjVaIiwiRGVsZXRlZEF0IjpudWxsLCJuYW1lIjoiYXJjYWRlIHNvbHV0aW9ucyBhZyIsInR5cGUiOiJhZG1pbiIsImFkZHJlc3MiOiJXaW5rZWxyaWVkc3RyYXNzZSAzNyIsInppcCI6IjYwMDMiLCJjaXR5IjoiTHV6ZXJuIiwiY29udGFjdF9lbWFpbCI6Im5zdEBhcmNhZGUuY2giLCJjb250YWN0X3Bob25lIjoiMDc5OTM2ODQ1MyIsInVzZXJzIjpudWxsLCJwYXJlbnRfY29tcGFueV9pZCI6MH0sImV4cCI6MjU1NzMwNDkyNywidXNlciI6eyJJRCI6InRlc3QifX0.Y-IcuFrzGvVDUFR3HpfE3rrhLhPlV2LXqSuXVBYZdMg`

const pathToPublicKey = `../test/public.pem`
const pathToPrivateKey = `../test/key.pem`

func TestGetDataFromTokenByKey(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/g/", nil)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtKeyHs256))
	req2 := httptest.NewRequest(http.MethodGet, "/", nil)
	req2.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtNoUserKey))
	rec := httptest.NewRecorder()
	rec2 := httptest.NewRecorder()
	e := echo.New()
	e.Use(middleware.JWT([]byte("secret")))
	c := e.NewContext(req, rec)
	c2 := e.NewContext(req2, rec2)
	type args struct {
		ctx echo.Context
		key string
	}
	tests := []struct {
		name    string
		args    args
		nil     bool
		wantErr bool
	}{
		{
			name: "TestGetDataFromTokenByKey",
			args: args{
				ctx: c,
				key: "user",
			},
			nil:     false,
			wantErr: false,
		},
		{
			name: "TestGetDataFromTokenByKeyNotFound",
			args: args{
				ctx: c2,
				key: "abc",
			},
			nil:     true,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := middleware.JWT([]byte("secret"))(func(i echo.Context) error {
				_, err := GetDataFromTokenByKey(tt.args.ctx, tt.args.key)
				return err
			})(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("CreateNotification() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCheckRole(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodDelete, "/", nil)
	rec := httptest.NewRecorder()
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtKeyHs256))
	c := e.NewContext(req, rec)
	req2 := httptest.NewRequest(http.MethodDelete, "/", nil)
	rec2 := httptest.NewRecorder()
	req2.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtNoAdminKey))
	c2 := e.NewContext(req2, rec2)
	req3 := httptest.NewRequest(http.MethodDelete, "/", nil)
	rec3 := httptest.NewRecorder()
	req3.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtNoUserKey))
	c3 := e.NewContext(req3, rec3)
	type args struct {
		ctx     echo.Context
		role    []string
		userKey string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "TestCheckRole",
			args: args{
				ctx:     c,
				userKey: "user",
				role:    []string{"admin"},
			},
			wantErr: false,
		},
		{
			name: "TestCheckRoleFalseRole",
			args: args{
				ctx:     c2,
				role:    []string{"admin"},
				userKey: "user",
			},
			wantErr: true,
		},
		{
			name: "TestCheckNoUser",
			args: args{
				ctx:     c3,
				role:    []string{"admin"},
				userKey: "user",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := middleware.JWT([]byte("secret"))(func(i echo.Context) error {
				return CheckRole(i, tt.args.userKey, tt.args.role...)
			})(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("CheckRole() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCheckTenantIDFromHTTPHeader(t *testing.T) {
	e := echo.New()
	e.Use(CheckTenantIDFromHTTPHeader)
	e.GET("/", func(context echo.Context) error {
		return context.NoContent(http.StatusOK)
	})
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req2 := httptest.NewRequest(http.MethodGet, "/", nil)
	req3 := httptest.NewRequest(http.MethodGet, "/", nil)
	req2.Header.Set("X-Tenant-ID", "invalid")
	req3.Header.Set("X-Tenant-ID", "1")
	rec := httptest.NewRecorder()
	rec2 := httptest.NewRecorder()
	rec3 := httptest.NewRecorder()
	e.ServeHTTP(rec, req)
	e.ServeHTTP(rec2, req2)
	e.ServeHTTP(rec3, req3)
	t.Run("TestSetUserToAPM", func(test *testing.T) {
		assert.Equal(test, http.StatusBadRequest, rec.Code)
		assert.Equal(test, http.StatusBadRequest, rec2.Code)
		assert.Equal(test, http.StatusOK, rec3.Code)
	})
}

func TestHTTPErrorHandler(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	reqHead := httptest.NewRequest(http.MethodHead, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	cHead := e.NewContext(reqHead, rec)
	internal := echo.NewHTTPError(http.StatusInternalServerError, errors.New("test"))
	internal = internal.SetInternal(echo.NewHTTPError(http.StatusInternalServerError, "test"))
	type args struct {
		err error
		c   echo.Context
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestHTTPErrorHandler",
			args: args{
				err: gorm.ErrRecordNotFound,
				c:   c,
			},
		},
		{
			name: "TestHTTPErrorHandlerInternal",
			args: args{
				err: internal,
				c:   c,
			},
		},
		{
			name: "TestHTTPErrorHandlerNoHTTPError",
			args: args{
				err: errors.New("test"),
				c:   c,
			},
		},
		{
			name: "TestHTTPErrorHandlerHEAD",
			args: args{
				err: errors.New("test"),
				c:   cHead,
			},
		},
		{
			name: "TestHTTPErrorHandlerInvalidJSON",
			args: args{
				err: echo.NewHTTPError(http.StatusInternalServerError, "test", func() {}),
				c:   cHead,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			HTTPErrorHandler(tt.args.err, tt.args.c)
		})
	}
}

func TestEnforcer_Enforce(t *testing.T) {
	e := echo.New()
	enforcer := Enforcer{
		Enforcer: casbin.NewEnforcer("../test/rbac_model.conf", "../test/policy.csv"),
	}
	e.Use(middleware.JWT([]byte("secret")))
	e.Use(SetCurrentUserMiddleware)
	e.Use(enforcer.Enforce)
	e.GET("/restricted", func(context echo.Context) error {
		return context.NoContent(http.StatusOK)
	})
	req := httptest.NewRequest(http.MethodGet, "/restricted", nil)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtKeyHs256))
	req2 := httptest.NewRequest(http.MethodGet, "/", nil)
	req2.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtNoAdminKey))
	req3 := httptest.NewRequest(http.MethodGet, "/", nil)
	req2.Header.Set("X-Tenant-ID", "invalid")
	req3.Header.Set("X-Tenant-ID", "1")
	req3.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtNoAdminKey))
	rec := httptest.NewRecorder()
	rec2 := httptest.NewRecorder()
	rec3 := httptest.NewRecorder()
	e.ServeHTTP(rec, req)
	e.ServeHTTP(rec2, req2)
	e.ServeHTTP(rec3, req3)
	t.Run("TestEnforcer_Enforce", func(test *testing.T) {
		assert.Equal(test, http.StatusOK, rec.Code)
		assert.Equal(test, http.StatusForbidden, rec2.Code)
		assert.Equal(test, http.StatusForbidden, rec3.Code)
	})
}

func TestSetCurrentUser(t *testing.T) {
	e := echo.New()
	g := e.Group("/g")
	g.Use(middleware.JWT([]byte("secret")))
	g.Use(SetCurrentUserMiddleware)
	g.GET("/", func(context echo.Context) error {
		return context.NoContent(http.StatusOK)
	})
	req := httptest.NewRequest(http.MethodGet, "/g/", nil)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtKeyHs256))
	req2 := httptest.NewRequest(http.MethodGet, "/g/", nil)
	req2.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtKeyHs256))
	req3 := httptest.NewRequest(http.MethodGet, "/g/", nil)
	req3.Header.Set("Authorization", fmt.Sprintf("Bearer %s", JwtKeyHs256Invalid))
	rec := httptest.NewRecorder()
	rec2 := httptest.NewRecorder()
	rec3 := httptest.NewRecorder()
	e.ServeHTTP(rec, req)
	e.ServeHTTP(rec2, req2)
	e.ServeHTTP(rec3, req3)
	t.Run("TestSetCurrentUser", func(test *testing.T) {
		assert.Equal(test, http.StatusBadRequest, rec3.Code)
		assert.Equal(test, http.StatusOK, rec.Code)
		assert.Equal(test, http.StatusOK, rec2.Code)
	})
}

func TestCheckTenantID(t *testing.T) {
	e := echo.New()
	e.Use(CheckTenantIDFromHTTPHeader)
	e.Use(CheckTenantID)
	e.GET("/", func(context echo.Context) error {
		return context.NoContent(http.StatusOK)
	})
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req2 := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set("X-Tenant-ID", "a")
	req2.Header.Set("X-Tenant-ID", "1")
	rec := httptest.NewRecorder()
	rec2 := httptest.NewRecorder()
	e.ServeHTTP(rec, req)
	e.ServeHTTP(rec2, req2)
	t.Run("TestCheckTenantID", func(test *testing.T) {
		assert.Equal(test, http.StatusBadRequest, rec.Code)
		assert.Equal(test, http.StatusOK, rec2.Code)
	})
}

func TestPrepareJWTConfig1(t *testing.T) {
	rsa, _ := jwt.ParseRSAPublicKeyFromPEM([]byte(`-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAziL7AGtI+RVuX5s5yUWM
JKkVG+RGhO31jOoBK8ctKmKel/m/jrgzZpAlmdQg+1SakshSPbn1W+gxWhYiQ0dE
ZN++ugZLcOzx0Rrggub0SDdkHGVdsjiek7Z5D5lCdtww63l9TeV1XSiU+0AKiihn
Bks75SMco512sZZmCLiES9bnZfzc5LaeMLblOeIOrS0j07fQfI3Fjp5cD9u4LsVz
HlQlPjaKaq2HD5dWymzQy3zKAvYC6yfVffDNVFc1Vc/OygBavF1rZrNIpkY4BJ3s
wOur8HkTZOiq1JX762UREeG/T+crL6MiAZxi9jU+MwKyN4Oh4P3G4zsMqRl8aSng
RQIDAQAB
-----END PUBLIC KEY-----`))
	secretConfig := jwtmiddleware.DefaultJWTConfig
	secretConfig.SigningKey = "secret"
	rsaConfig := jwtmiddleware.Config{
		Skipper: func(c echo.Context) bool {
			return false
		},
		ContextKey:    "user",
		SigningKey:    rsa,
		SigningMethod: "RS256",
		TokenLookup:   "header:" + echo.HeaderAuthorization,
		AllowNoJWT:    true,
	}
	type args struct {
		jwtType       string
		jwtKey        string
		publicKeyPath string
	}
	tests := []struct {
		name    string
		args    args
		want    jwtmiddleware.Config
		wantErr bool
	}{
		{
			name: "TestPrepareJWTConfigSecret",
			args: args{
				jwtType: "secret",
				jwtKey:  "secret",
			},
			want:    secretConfig,
			wantErr: false,
		},
		{
			name: "TestPrepareJWTConfigRSA",
			args: args{
				jwtType:       "rsa",
				publicKeyPath: pathToPublicKey,
			},
			want:    rsaConfig,
			wantErr: false,
		},
		{
			name: "TestPrepareJWTConfigRSANotFound",
			args: args{
				jwtType:       "rsa",
				publicKeyPath: "pathToPublicKey",
			},
			want:    jwtmiddleware.Config{},
			wantErr: true,
		},
		{
			name: "TestPrepareJWTConfigRSAInvalidRSAFound",
			args: args{
				jwtType:       "rsa",
				publicKeyPath: "../../test/jwtmiddleware.json",
			},
			want:    jwtmiddleware.Config{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := PrepareJWTConfig(tt.args.jwtType, tt.args.jwtKey, tt.args.publicKeyPath)
			if (err != nil) != tt.wantErr {
				t.Errorf("PrepareJWTConfig() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Errorf("PrepareJWTConfig() got = %v, want %v, diff %v", got, tt.want, diff)
			}
		})
	}
}

func TestInitializeAuth(t *testing.T) {
	e := echo.New()
	type args struct {
		jwtType       string
		jwtKey        string
		publicKeyPath string
		casbinModel   string
		casbinPolicy  string
		apiKey        string
		group         *echo.Group
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "TestInitializeAuth",
			args: args{
				jwtType:       "rsa",
				jwtKey:        pathToPrivateKey,
				publicKeyPath: pathToPublicKey,
				casbinModel:   "",
				casbinPolicy:  "",
				apiKey:        "",
				group:         e.Group(""),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := InitializeAuth(tt.args.jwtType, tt.args.jwtKey, tt.args.publicKeyPath, tt.args.casbinModel, tt.args.casbinPolicy, tt.args.apiKey, tt.args.group); (err != nil) != tt.wantErr {
				t.Errorf("InitializeAuth() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAddRequestContextMiddleware(t *testing.T) {
	e := echo.New()
	db := &gorm.DB{}
	type args struct {
		e  *echo.Echo
		db *gorm.DB
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "TestAddRequestContextMiddleware",
			args: args{
				e:  e,
				db: db,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			AddRequestContextMiddleware(tt.args.e, tt.args.db)
		})
	}
}

func TestGetTenantIdFromContext(t *testing.T) {
	e := echo.New()
	ctx1 := e.NewContext(httptest.NewRequest(http.MethodGet, "/", nil), httptest.NewRecorder())
	ctx1.Set("tenant_id", uint(1))
	ctx2 := e.NewContext(httptest.NewRequest(http.MethodGet, "/", nil), httptest.NewRecorder())
	ctx2.Set("tenant_id", "0")
	ctx3 := e.NewContext(httptest.NewRequest(http.MethodGet, "/", nil), httptest.NewRecorder())
	type args struct {
		ctx echo.Context
	}
	tests := []struct {
		name string
		args args
		want uint
	}{
		{
			name: "TestGetTenantIdFromContext",
			args: args{
				ctx: ctx1,
			},
			want: 1,
		},
		{
			name: "TestGetTenantIdFromContextNoUint",
			args: args{
				ctx: ctx2,
			},
			want: 0,
		},
		{
			name: "TestGetTenantIdFromContextNoTenantId",
			args: args{
				ctx: ctx3,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetTenantIdFromContext(tt.args.ctx); got != tt.want {
				t.Errorf("GetTenantIdFromContext() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetCurrentUser(t *testing.T) {
	e := echo.New()
	ctx1 := e.NewContext(httptest.NewRequest(http.MethodGet, "/", nil), httptest.NewRecorder())
	ctx1.Set("user", user.User{Model: gorm.Model{ID: 1}})
	ctx2 := e.NewContext(httptest.NewRequest(http.MethodGet, "/", nil), httptest.NewRecorder())
	ctx2.Set("user", "0")
	type args struct {
		ctx echo.Context
	}
	tests := []struct {
		name string
		args args
		want user.User
	}{
		{
			name: "TestGetCurrentUser",
			args: args{
				ctx: ctx1,
			},
			want: user.User{Model: gorm.Model{ID: 1}},
		},
		{
			name: "TestGetCurrentUserNotFound",
			args: args{
				ctx: ctx2,
			},
			want: user.User{
				Role: "anonymous",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetCurrentUser(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCurrentUser() = %v, want %v", got, tt.want)
			}
		})
	}
}
